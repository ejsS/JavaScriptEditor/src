package org.colos.ejs.osejs.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.colos.ejs.osejs.Osejs;
import org.colos.ejs.osejs.OsejsCommon;
import org.colos.ejss.xml.JSObfuscator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.opensourcephysics.tools.JarTool;

public class AppEJSModel {
  static private final ResourceUtil res = new ResourceUtil("Resources");
  static private ArrayList<TwoStrings> relocatedFiles = getRelocatedFiles();

  private String mModelTitle = "";
  private String mModelAuthor ="";
  private String mModelAuthorImage ="";
  private String mLogoImage="";
  private int mSimulationPageIndex = 0;
  private ArrayList<TwoStrings> mModelPages = new ArrayList<TwoStrings>();

  // ----------------------------------- 
  // Setters and getters
  // -----------------------------------

  protected void setTitle(String _title) {
    mModelTitle = _title;
  }

  public String getTitle() {
    if (mModelTitle.length()<=0) mModelTitle = "Untitled";
    return mModelTitle; 
  }

  public String getAuthor() {
    if (mModelAuthor.length()<=0) mModelAuthor = "Unknown author";
    return mModelAuthor; 
  }

  public String getLogoImageFilename() { return mLogoImage; }

  public ArrayList<TwoStrings> getModelPages() { return mModelPages; }

  public int getSimulationPageIndex() { return mSimulationPageIndex; }

  public java.util.List<TwoStrings> getAuthorInfo() {
    if (mModelAuthor.length()<=0) mModelAuthor = "Unknown author";
    ArrayList<TwoStrings> authorInfo = new ArrayList<TwoStrings>();
    String[] names = mModelAuthor.split(";");
    String[] images = mModelAuthorImage.split(";");
    for (int i=0; i<names.length; i++) {
      authorInfo.add(new TwoStrings(names[i],(i<images.length) ? images[i] : null));
    }
    return authorInfo;
  }

  // ----------------------------------- 
  // Add files required by these models
  // -----------------------------------

  protected void addFilesToTargetFolder(Osejs _ejs, File _targetFolder) {
    File targetAssetsFolder = new File(_targetFolder,"src/assets");

    // Copy CSS files
    File userLibraryCSS = new File(_ejs.getConfigDirectory(),OsejsCommon.EJS_LIBRARY_DIR_PATH+"/css/ejss.css");
    if (userLibraryCSS.exists()) {
      if (!FileUtils.copy(userLibraryCSS, new File(targetAssetsFolder,"ejs_css/ejss.css"))) 
        System.err.println(res.getString("Osejs.File.SavingError")+"\n"+userLibraryCSS.getAbsolutePath());             
    }
    else {
      File libraryCSS = new File(_ejs.getBinDirectory(),OsejsCommon.CONFIG_DIR_PATH+"/"+OsejsCommon.EJS_LIBRARY_DIR_PATH+"/css/ejss.css");
      if (libraryCSS.exists()) {
        if (!FileUtils.copy(libraryCSS, new File(targetAssetsFolder,"ejs_css/ejss.css"))) 
          System.err.println(res.getString("Osejs.File.SavingError")+"\n"+libraryCSS.getAbsolutePath());             
      } 
    }
    // Copy JavaScript files
    ArrayList<TwoStrings> jsFiles = new ArrayList<TwoStrings>();
    jsFiles.add(new TwoStrings("scripts/textresizedetector.js","ejs_js/textresizedetector.js")); 
    jsFiles.add(new TwoStrings(JSObfuscator.LIB_MIN_FILENAME,  "ejs_js/"+JSObfuscator.LIB_MIN_FILENAME));

    File javascriptLibDir = new File(_ejs.getBinDirectory(),"javascript/lib");
    for (TwoStrings ts : jsFiles) {
      File srcFile = new File (javascriptLibDir,ts.getFirstString());
      if (!FileUtils.copy(srcFile, new File(targetAssetsFolder,ts.getSecondString()))) 
        System.err.println(res.getString("Osejs.File.SavingError")+"\n"+srcFile.getAbsolutePath());
    }
  }

  // -------------------------- 
  // Process generated files
  // -------------------------- 

  static protected boolean isEJSModel(File _tmpModelFolder) {
    File metadataFile = new File(_tmpModelFolder,"_metadata.txt");
    return metadataFile.exists();
  }

  /**
   * Simple usage when the stand-alone model was generated directly 
   * in the model folder, i.e. no need to copy. 
   * @param _targetFolder
   * @param _tmpModelFolder
   */
  protected void processModel(File _targetFolder, File _tmpModelFolder) {
    processModel(_targetFolder,_tmpModelFolder,null,null,null,true);
  }

  protected void processModel(File _targetFolder, File _tmpModelFolder, 
      File _permanentModelFolder, String _modelName, String _modelZipName, 
      boolean _includeHTMLpages) {
    
    File metadataFile = new File(_tmpModelFolder,"_metadata.txt");
    if (!metadataFile.exists()) return; // This is not an ejss_model file

    String lastPageTitle=null;
    String htmlMain=null;
    StringTokenizer tkn = new StringTokenizer(FileUtils.readTextFile(metadataFile, null),"\n");
    while (tkn.hasMoreTokens()) {
      String line = tkn.nextToken();
      if (line.startsWith("title:")) {
        mModelTitle = line.substring(6).trim();
      }
      else if (line.startsWith("logo-image:")) {
        mLogoImage = line.substring(11).trim();
      }
      else if (line.startsWith("author:")) {
        if (mModelAuthor.length()>0) mModelAuthor += ";"+line.substring(7).trim();
        else mModelAuthor = line.substring(7).trim();
      }
      else if (line.startsWith("author-image:")) {
        if (mModelAuthorImage.length()>0) mModelAuthorImage += ";"+line.substring(13).trim();
        else mModelAuthorImage = line.substring(13).trim();
      }
      else if (line.startsWith("html-main:")) {
        htmlMain = line.substring(11).trim();
      }
      else if (line.startsWith("page-title:")) {
        lastPageTitle = line.substring(11).trim();
      }
      else if (line.startsWith("page-index:")) {
        String pageIndex = line.substring(11).trim();
        boolean isSimulation = (pageIndex.endsWith("_Simulation.html") || pageIndex.endsWith("_Simulation.xhtml"));
        if (isSimulation || _includeHTMLpages) {
          File pageFile = new File(_tmpModelFolder,pageIndex);
          String targetFilename = (isSimulation && _modelName!=null) ? _modelName+"_"+ pageIndex : pageIndex;
          mModelPages.add(new TwoStrings(lastPageTitle, "assets/ejs_pages/"+targetFilename));
          if (isSimulation) { // process the file and see if there is a js file
            mSimulationPageIndex = mModelPages.size()-1;
            TwoStrings ts = FileUtils.getPlainNameAndExtension(pageFile);
            File jsFile = new File(_tmpModelFolder,ts.getFirstString()+".js");
            if (jsFile.exists()) AppSingle.processReferences(jsFile);
            else AppSingle.processReferences(pageFile);
          }
          else AppSingle.processReferences(pageFile);
          relocateFiles(pageFile,isSimulation,relocatedFiles);          
        }
      } // Metadata processed
    } // end of processing metadata

    processModelFolder(_targetFolder, _permanentModelFolder, _tmpModelFolder, htmlMain, mModelTitle);
  }

  // Process model file
  static private void processModelFolder(File _targetFolder, 
      File _permanentModelFolder, File _tmpModelFolder, 
      String _htmlMain, String _modelTitle) {
    
    String tmpModelFolderPath = FileUtils.getPath(_tmpModelFolder); 
    String commonScriptFilename = "_ejs_library/"+JSObfuscator.sCOMMON_SCRIPT;
    ArrayList<String> ignoreFilesWithName = new ArrayList<String>();
    ignoreFilesWithName.add(".DS_Store");
    ignoreFilesWithName.add("_ejs_README.txt");

    for (File file : JarTool.getContents(_tmpModelFolder)) {
      String filename = FileUtils.getRelativePath(file, tmpModelFolderPath, false);
      if (filename.startsWith("/")) filename = filename.substring(1);
      boolean mustDelete = false;
      String extension = FileUtils.getPlainNameAndExtension(file).getSecondString().toLowerCase();

      // Delete EjsS generated files that are not needed 
      if (_htmlMain!=null && filename.equals(_htmlMain)) {
        String content = FileUtils.readTextFile(file, OsejsCommon.getUTF8());
        if (content.indexOf("<frameset cols=")>=0) mustDelete = true;
      }
      else if (filename.endsWith("_Contents.html") || filename.endsWith("_Contents.xhtml")) {
        String content = FileUtils.readTextFile(file, OsejsCommon.getUTF8());
        if (content.indexOf("<div class=\"contents\"")>=0) mustDelete = true;
      }
      else if (filename.endsWith("_opensocial.xml")) {
        String content = FileUtils.readTextFile(file, OsejsCommon.getUTF8());
        if (content.indexOf("<Module>")>0 && content.indexOf("<ModulePrefs")>0) mustDelete = true;
      }
      else if (filename.endsWith(commonScriptFilename)) { // replace common script with the common script needed
        EPub.combineCommonScript(file, new File(_targetFolder,"src/assets/ejs_js/common_script.js"), filename, _modelTitle);
        mustDelete = true;
      }
      else if (filename.equals("_ejs_library/css/ejss.css")) { // replace system CSS with user defined CSS
        if (!FileUtils.copy(file, new File(_targetFolder,"src/assets/ejs_css/ejss.css"))) 
          System.err.println(res.getString("Osejs.File.SavingError")+"\n"+file.getAbsolutePath());             
        mustDelete = true;
      }
      else if (filename.startsWith("_ejs_library/")) { // remove all library files, common_script.js will do the rest
        mustDelete = true;
      }
      else if (extension.equals("html") || extension.equals("xhtml")) {
        //        if (!modelPagesSet.contains(filename)) { // There may be XHTML as HTMLArea content
        //          System.err.println(res.getString("Package.EPUBIgnoringFile")+ ": "+filename+ " ("+modelTitle+")");
        //          mustDelete = true;
        //        }
      }
      else if (OsejsCommon.isEJSfile(file)) mustDelete = true;
      else {
        for (String ignoreFilename : ignoreFilesWithName) {
          if (filename.equals(ignoreFilename)) mustDelete = true;
        }
      }
      if (mustDelete) {
        file.delete();
        continue;
      }
      if (_permanentModelFolder!=null) {
        String targetFilename;
        String originalName = FileUtils.getRelativePath(file, tmpModelFolderPath, false);
        if (originalName.endsWith("_Simulation.xhtml")) targetFilename = _tmpModelFolder.getName()+"_"+ originalName;
        else targetFilename = originalName;
        File targetFile = new File(_permanentModelFolder,targetFilename);
        if (targetFile.exists()) { // repeated, ignore
          String removePrefix = filename.substring(filename.indexOf('/')+1);
          if (!removePrefix.startsWith("_ejs_library")) {
            System.err.println(res.getString("Package.EPUBIgnoringFile")+ ": "+targetFilename + " ("+_modelTitle+")");
            //              repeatedFiles.add(targetFilename);
          }
          continue;
        }
        else JarTool.copy(file,targetFile);
      }
    } // end processing model folder contents
  }

  //---------------------------------------
  // Tools to relocate references   
  //---------------------------------------

  static private ArrayList<TwoStrings> getRelocatedFiles() {
    ArrayList<TwoStrings> relocateFiles = new ArrayList<TwoStrings>();
    relocateFiles.add(new TwoStrings("_ejs_library/images/Gyroscope.gif",         "../ejs_img/Gyroscope.png")); 
    relocateFiles.add(new TwoStrings("_ejs_library/css/ejss.css",                 "../ejs_css/ejss.css"));
    relocateFiles.add(new TwoStrings("_ejs_library/css/ejsPage.css",              "../ejs_css/ejss.css"));
    relocateFiles.add(new TwoStrings("_ejs_library/css/ejsSimulation.css",        "../ejs_css/ejss.css")); 
    relocateFiles.add(new TwoStrings("_ejs_library/scripts/textresizedetector.js",   "../ejs_js/textresizedetector.js")); 
    relocateFiles.add(new TwoStrings("_ejs_library/"+JSObfuscator.sCOMMON_SCRIPT,    "../ejs_js/common_script.js"));
    relocateFiles.add(new TwoStrings("_ejs_library/"+JSObfuscator.LIB_MIN_FILENAME,  "../ejs_js/"+JSObfuscator.LIB_MIN_FILENAME));
    return relocateFiles;
  }

  static private boolean changeProperty(Element element, String property, ArrayList<TwoStrings> commonFiles) {
    String value = element.attr(property);
    for (TwoStrings ts : commonFiles) {
      if (ts.getFirstString().equals(value)) {
        element.attr(property,ts.getSecondString());
        return true;
      }
    }
    return false;
  }

  /**
   * Relocate files references
   * @param _refEntry
   * @param _file
   * @param _counterRef
   */
  static private void relocateFiles(File file, boolean isSimulation, ArrayList<TwoStrings> commonFiles) {
    System.out.println("Must process CSS and JS for file: "+file.getName());
    try {
      Document doc = Jsoup.parse(FileUtils.readTextFile(file, null));
      Element head = doc.head();
      Elements imports = head.select("link[href]");
      boolean changed = false;
      for (Element link : imports) {
        if ("stylesheet".equalsIgnoreCase(link.attr("rel")) || "text/css".equalsIgnoreCase(link.attr("type")) ) {
          if (changeProperty(link,"href",commonFiles)) changed = true;
          else {
            String href = link.attr("href");
            if (href.equals("ejss.css") || href.endsWith("/ejss.css")) {
              link.attr("href","../ejs_css/ejss.css");
              changed = true;
            }
          }
        }
      }
      Elements scripts = head.select("script[src]");
      for (Element script : scripts) {
        if (changeProperty(script,"src",commonFiles)) {
          script.attr("type","text/javascript");
          changed = true; 
        }
      }
      if (isSimulation) {
        Element metadata = doc.body().getElementById("metadata");
        if (metadata!=null) {
          metadata.remove();
          changed = true;
        }
      }
      if (changed) {
        doc.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml); //This will ensure the validity
        FileUtils.saveToFile(file, null, doc.toString());     
      }

    } catch (Exception e) {
      System.err.println("Warning: Error trying to modify CSS and JS links for file : "+file.getName());
      e.printStackTrace();
    }
  }

}
