package org.colos.ejs.osejs.utils;

import java.io.File;

import org.colos.ejs.osejs.Osejs;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.opensourcephysics.tools.JarTool;

public class AppPhysletModel extends AppEJSModel {
  static private final ResourceUtil res = new ResourceUtil("Resources");

  // ----------------------------------- 
  // Setters and getters
  // -----------------------------------

  public String getAuthor() { return "Wolfgang Christian; Mario Belloni"; }

  public String getLogoImageFilename() { return "assets/physlets_img/logo.png"; }

  // ----------------------------------- 
  // Add files required by these models
  // -----------------------------------

  protected void addFilesToTargetFolder(Osejs _ejs, File _targetFolder) {
    File templateDir = new File(_ejs.getBinDirectory(),"javascript/APP_PHYSLETS/IONIC2");    
    FileUtils.copyDirectory(templateDir, _targetFolder,true); 
  }

  // ---------------- 
  // Process generated files

  static protected boolean isPhysletModel(File _tmpModelFolder, String _modelZipName) {
    File macosFolder = new File(_tmpModelFolder,"__MACOSX");
    if (macosFolder.exists() && macosFolder.isDirectory()) _tmpModelFolder = new File(_tmpModelFolder,_modelZipName);
    File swingjsFolder = new File(_tmpModelFolder,"swingjs");
    return swingjsFolder.exists() && swingjsFolder.isDirectory();
  }

  protected void processModel(File _tmpFolder, File _tmpModelFolder, 
      File _permanentModelFolder, String _modelName, String _modelZipName, 
      boolean _includeHTMLpages) {
    
    File macosFolder = new File(_tmpModelFolder,"__MACOSX");
    if (macosFolder.exists() && macosFolder.isDirectory()) _tmpModelFolder = new File(_tmpModelFolder,_modelZipName);

    File swingjsFolder = new File(_tmpModelFolder,"swingjs");
    if (! (swingjsFolder.exists() && swingjsFolder.isDirectory()) ) return; // This is not an ejss_model file

    String tmpModelFolderPath = FileUtils.getPath(_tmpModelFolder); 
    for (File file : JarTool.getContents(_tmpModelFolder)) {
      String filename = FileUtils.getRelativePath(file, tmpModelFolderPath, false);
      if (filename.startsWith("/")) filename = filename.substring(1);
      String extension = FileUtils.getPlainNameAndExtension(file).getSecondString().toLowerCase();

      // Delete EjsS generated files that are not needed 
      if (extension.equals("html") || extension.equals("xhtml")) {
        if (!(filename.startsWith("resources/") || filename.startsWith("swingjs")) ) {
          setTitle(file.getParentFile().getName());
          getModelPages().add(new TwoStrings(findTitle(file),"assets/physlets_pages/"+filename));
          AppSingle.processReferences(file);
        }
      }
      if (_permanentModelFolder!=null) {
        String targetFilename = FileUtils.getRelativePath(file, tmpModelFolderPath, false);
        File targetFile = new File(_permanentModelFolder,targetFilename);
        if (targetFile.exists()) { // repeated, ignore
          String removePrefix = filename.substring(filename.indexOf('/')+1);
          if (!removePrefix.startsWith("swingjs")) {
            System.err.println(res.getString("Package.EPUBIgnoringFile")+ ": "+targetFilename + " ("+_modelName+")");
            //              repeatedFiles.add(targetFilename);
          }
          continue;
        }
        else JarTool.copy(file,targetFile);
      }
    }
  }

  //---------------------------------------
  // Tools    
  //---------------------------------------

  static private String findTitle(File _htmlFile) {
    try {
      Document doc = Jsoup.parse(FileUtils.readTextFile(_htmlFile, null));
      String title = doc.title();
      if (title!=null) return title;
    } catch (Exception e) {
      System.err.println("Warning: Error trying to read title of file : "+_htmlFile.getName());
      e.printStackTrace();
    }
    return "Unnamed Physlet";
  }

}
