package org.colos.ejs.osejs.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;
import org.colos.ejs.osejs.GenerateJS;
import org.colos.ejs.osejs.Osejs;
import org.colos.ejs.osejs.OsejsCommon;
import org.colos.ejss.xml.JSObfuscator;
import org.opensourcephysics.tools.JarTool;

public class AppSingle {
  static private final ResourceUtil res = new ResourceUtil("Resources");
  
  /**
   * Compresses a compiled simulation into material appropriated to create an Ionic App
   * @param _ejs
   */
  static public void convertToSingleApp(Osejs _ejs) {
    // Save file
    File xmlFile = _ejs.getCurrentXMLFile();
    if (!xmlFile.exists()) {
      JOptionPane.showMessageDialog(_ejs.getMainPanel(), res.getString("Package.NoSimulations"),
          res.getString("Osejs.File.Error"), JOptionPane.INFORMATION_MESSAGE);
      return;
    }
    if (_ejs.checkChangesAndContinue(false) == false) return; // The user canceled the action
    
    // Choose App options
    AppSingleDialog.AppSingleOptions optionsInfo = AppSingleDialog.getSingleAppOptions(_ejs, _ejs.getMainFrame());
    if (optionsInfo==null) return;

    // Choose existing IONIC project
    File targetFile = null;
    String targetName = "ejss_XXX_"+FileUtils.getPlainNameAndExtension(xmlFile).getFirstString() + ".none";
    targetFile = new File(_ejs.getExportDirectory(), targetName);
    targetName = FileChooserUtil.chooseFoldername(_ejs, targetFile, "", new String[]{""}, false);
    if (targetName==null) return;

    targetFile = new File(targetName);
    if (!targetFile.exists() || !targetFile.isDirectory() || ! new File(targetFile,"src/app").exists()) {
      int selected = JOptionPane.showConfirmDialog(_ejs.getMainPanel(),
          res.getString("App.NotAnApp")+"\n"+res.getString("SingleApp.WantHelp"),
          res.getString("Information"), JOptionPane.YES_NO_OPTION);
      if (selected == JOptionPane.YES_OPTION) {
        String urlPage = "http://www.um.es/fem/EjsWiki/Main/CreatingSingleApps";
        org.opensourcephysics.desktop.OSPDesktop.displayURL(urlPage);
      }
      return;
    }
    
    // Here we go
    _ejs.getOutputArea().message("App.ModifyingApp",targetFile.getAbsolutePath());      

    // Temporary folder that will contain all generated files
    File tmpFolder = prepareTargetFolder(_ejs,"javascript/APP_SINGLE/IONIC2/");

    // Target App folders
    File modelFolder = new File(tmpFolder,"src/assets/ejs_pages");
    modelFolder.mkdirs();

    { // generate the simulation from EjsS
      // needed HTML simulation to avoid errors in Ionic serve
      Boolean previous = JSObfuscator.isGenerateXHTML();
      JSObfuscator.setGenerateXHTML(false);
      String simulationName = _ejs.getSimInfoEditor().getSimulationName();
      if (simulationName==null) simulationName = _ejs.getCurrentXMLFilename();
      if (!GenerateJS.prepackageXMLSimulation(_ejs, xmlFile, simulationName, modelFolder, true, null)) { // true: simplified, null: Not SCORM
        _ejs.getOutputArea().println(res.getString("App.AppNotModified")+ " : "+targetFile.getName());
        JSObfuscator.setGenerateXHTML(previous);
        return;
      }
      JSObfuscator.setGenerateXHTML(previous);      
    }
    
    if (!AppEJSModel.isEJSModel(modelFolder)) {
      _ejs.getOutputArea().println("Warning: generated files do not include an EJS Javascript model. Ignored!");
      JOptionPane.showMessageDialog(_ejs.getMainPanel(),res.getString("Package.JarFileNotCreated"),
          res.getString("Osejs.File.Error"), JOptionPane.INFORMATION_MESSAGE);
      JarTool.remove(tmpFolder);
      return;
    }

    AppEJSModel appModel = new AppEJSModel();
    appModel.addFilesToTargetFolder(_ejs, tmpFolder);
    appModel.processModel(tmpFolder,modelFolder);
    
   // Create main files
    createAppStructureFile(_ejs, tmpFolder, appModel, optionsInfo);
    
    // Copy the temporary folder contents to the IONIC project folder
    FileUtils.copyDirectory(tmpFolder, targetFile,true);
    JarTool.remove(tmpFolder);
    _ejs.getOutputArea().println(res.getString("App.AppModified")+ " : "+targetFile.getAbsolutePath());
  }
  
  static protected File prepareTargetFolder(Osejs _ejs, String _templateFilename) {
    // Create a working temporary directory
    File tempWorkingFolder=null;
    try {
      tempWorkingFolder = File.createTempFile("EjsPackage", ".tmp", _ejs.getExportDirectory()); // Get a unique name for our temporary directory
      tempWorkingFolder.delete();        // remove the created file
      tempWorkingFolder.mkdirs();
    }
    catch (Exception exc) {  
      exc.printStackTrace();
      JOptionPane.showMessageDialog(_ejs.getMainPanel(),res.getString("Package.JarFileNotCreated"),
          res.getString("Osejs.File.Error"), JOptionPane.INFORMATION_MESSAGE);
      JarTool.remove(tempWorkingFolder);
      return null;
    }
    
//    File templateDir = new File(_ejs.getBinDirectory(),_templateFilename);
//    File targetSrcFolder = new File(tempWorkingFolder,"src");
    
    FileUtils.copyDirectory(new File(_ejs.getBinDirectory(),_templateFilename+"/src"), new File(tempWorkingFolder,"src"),true); 
//    FileUtils.copyDirectory(new File(templateDir,_srcFilename), targetSrcFolder,true); 

    return tempWorkingFolder;
  }

  static public void createAppStructureFile(Osejs _ejs, File _workingFolder,
      AppEJSModel _appModel, AppSingleDialog.AppSingleOptions _options) {

    StringBuffer buffer = new StringBuffer();
    buffer.append("var book_title = \""+_appModel.getTitle()+"\";\n");
    buffer.append("var book_header = \""+_appModel.getTitle()+"\";\n");
    buffer.append("var book_author = \"");
    for (TwoStrings ts : _appModel.getAuthorInfo()) {
      buffer.append("<h2>");
      String authorImage = ts.getSecondString();
      if (authorImage!=null && authorImage.trim().length()>0) buffer.append("<img src='assets/ejs_pages/"+authorImage.trim()+"' alt='Photo of "+ts.getFirstString()+"' /> ");
      buffer.append(ts.getFirstString()+"</h2>");
    }
    buffer.append("\";\n");
    buffer.append("var book_abstract = \"");
    String abstractTxt = _ejs.getSimInfoEditor().getAbstract();
    if (abstractTxt.length()>0) {
      boolean hasTag = (abstractTxt.charAt(0)=='<'); 
      if (!hasTag) {
        buffer.append("<p>");
        buffer.append(abstractTxt.replaceAll("(\r\n|\n\r|\r|\n)", "<br />"));
        buffer.append("</p>");
      } else {
        buffer.append(abstractTxt.replaceAll("(\r\n|\n\r|\r|\n)", " "));
      }
    }
    buffer.append("\";\n");
    buffer.append("var book_copyright = \"");
    String copyrightTxt = _ejs.getSimInfoEditor().getCopyright();
    if (copyrightTxt.length()>0) {
      boolean hasTag = (copyrightTxt.charAt(0)=='<'); 
      if (!hasTag) {
        buffer.append("<p>");
        buffer.append(copyrightTxt.replaceAll("(\r\n|\n\r|\r|\n)", "<br />"));
        buffer.append("</p>");
      } else {
        buffer.append(copyrightTxt.replaceAll("(\r\n|\n\r|\r|\n)", " "));
      }
    }
    buffer.append("\";\n");
    
    String bookLogo =  _appModel.getLogoImageFilename();
    if (bookLogo!=null && bookLogo.trim().length()>0) 
      buffer.append("var book_logo = \"assets/ejs_pages/"+bookLogo.trim()+"\";\n\n");
    else 
      buffer.append("var book_logo = null;\n\n");
    
    buffer.append("var book_menu_title = \""+res.getString("Package.Contents")+"\";\n");
    buffer.append("var book_toc_title = \""+res.getString("Package.TableOfContents")+"\";\n");
    buffer.append("var book_about_title = \""+res.getString("Package.About")+"\";\n");
    buffer.append("var book_copyright_title = \""+res.getString("Package.Copyright")+"\";\n\n");

    buffer.append("var book_simulation_first = "+_options.isSimulationFirst()+";\n");
    buffer.append("var book_simulation_index = "+_appModel.getSimulationPageIndex()+";\n\n");
    buffer.append("var book_type = \"");
    switch(_options.getTemplate()) {
      case CARD:   buffer.append("card");   break;
      case SLIDES: buffer.append("slides"); break;
      case TABS:   buffer.append("tabs");   break;
      default:
      case MENU:   buffer.append("menu");   break;
    }
    buffer.append("\";\n");
    buffer.append("var book_full_screen = "+_options.isFullScreen()+";\n");
    buffer.append("var book_locking = ");
    switch (_options.getScreenLocking()) {
      case PORTRAIT  : buffer.append("'portrait';\n"); break;
      case LANDSCAPE : buffer.append("'landscape';\n"); break;
      default :
      case NONE : buffer.append("'none';\n"); break;
    }

    buffer.append("\n");
    buffer.append("var book_toc = [");
    int counter = 1;
    ArrayList<TwoStrings> pagesList = _appModel.getModelPages(); 
    for (TwoStrings page : pagesList) {
      if (counter==1) buffer.append("\n");
      else buffer.append(",\n");
      buffer.append("  {\n");
      buffer.append("    title: \""+page.getFirstString()+"\",\n");
      buffer.append("    url: \""+page.getSecondString()+"\"\n");
      buffer.append("  }");
      counter++;
    }
    buffer.append("\n];\n");

    File outputFile = new File (_workingFolder,"src/assets/ejs_js/book.js");
    try {
      FileUtils.saveToFile(outputFile, OsejsCommon.getUTF8(), buffer.toString());
    }
    catch (Exception exc) {
      exc.printStackTrace();
      JOptionPane.showMessageDialog(_ejs.getMainPanel(), 
          res.getString("Osejs.File.SavingError")+"\nsrc/assets/ejs_js/pages.js", 
          res.getString("Warning"), JOptionPane.ERROR_MESSAGE);
    }
  }

//---------------------------------------
// Tools to process URL links
//---------------------------------------
  /**
   * Finds all Anchor tags and adds them to a RefEntry list
   * @param _refEntry
   * @param _file
   * @param _counterRef
   */
  static public void processReferences(File _file) {
//    System.out.println("Must process references for file: "+_file.getName());
    if (!_file.exists()) {
      System.err.println("References file does not exist: "+_file.getName());
      return;
    }
    String anchorTag = "<a";
    int tagLength = anchorTag.length();
    StringBuffer buffer = new StringBuffer ();

    String source = FileUtils.readTextFile(_file, null);
    int start = source.indexOf(anchorTag);
    boolean changed = false;
    while (start>=0) {
      buffer.append(source.substring(0,start));
      source = source.substring(start);
      buffer.append(anchorTag);

      char nextChar = source.charAt(tagLength); 
      if (nextChar==' ' || nextChar=='\t' || nextChar=='\n') {
        int end = findAnchorEnd(source);
        if (end<0) { // This is actually a syntax error
          System.err.println("Warning: Anchor syntax not ended with </a> in file : "+_file.getName());
          return;
        }
        if (replaceAnchorLink(buffer,source.substring(2,end),_file.getName())) changed = true;
        source = source.substring(end);
      }
      else { // This seems to be another <a tag, such as <annotation
        System.err.println("Warning: <a is not an anchor tag in file : "+_file.getName());
        source = source.substring(tagLength);
      }
      start = source.indexOf(anchorTag);
    }
    if (!changed) return;
    buffer.append(source);
    try {
      FileUtils.saveToFile(_file, null, buffer.toString());
//      System.err.println("REFERENCES changed in file : "+_file.getName());
    } catch (IOException e) {
      System.err.println("Warning: Error trying to save corrected Anchor tags for file : "+_file.getName());
      e.printStackTrace();
    }
  }

  static private boolean replaceAnchorLink(StringBuffer buffer, String notEndedAnchorText, String _filename) {
    String hRef = "href";
    int index = notEndedAnchorText.indexOf(hRef);
    if (index>=0) {
      String prev = notEndedAnchorText.substring(0,index);
      String rest = notEndedAnchorText.substring(index+4).trim();
      if (rest.startsWith("=") && rest.length()>2) {
        rest = rest.substring(1).trim();
        String delim;
        if (rest.startsWith("\\\"")) {
          delim = "\\\"";
          rest = rest.substring(2);
        }
        else {
          delim = rest.substring(0, 1);
          rest = rest.substring(1);
        }
        int end = rest.indexOf(delim);
        if (end>=0) { // found correctly
          String url = rest.substring(0,end);
          if (url.toLowerCase().startsWith("javascript:")) {
            buffer.append(notEndedAnchorText);
            return false;
          }
          rest = rest.substring(end+delim.length());
          index = rest.lastIndexOf('>');
          if (index>=0) { // Find title
            buffer.append(prev);
            buffer.append("href="+delim+delim+" onclick="+delim+"parent.selectReference('"+url+"')"+delim);
            buffer.append(rest);
            System.err.println ("REFERENCE: for "+url+" processed ok in file "+_filename);
            return true;
          }
        }
      }
    }
    buffer.append(notEndedAnchorText);
    return false;
  }

  static private int findAnchorEnd(String source) {
    int index = source.indexOf("</");
    try {
      while (index>=0) {
        String rest = source.substring(index+2).trim();
        StringTokenizer tkn = new StringTokenizer(rest," \t");
        String firstToken = tkn.nextToken(); 
        if (firstToken.startsWith("a>")) return index;
        if (firstToken=="a" && tkn.nextToken().startsWith(">")) return index;
        index = source.indexOf(source, index+2);
      }
    } catch(Exception exc) { }
    return -1;
  }


}
