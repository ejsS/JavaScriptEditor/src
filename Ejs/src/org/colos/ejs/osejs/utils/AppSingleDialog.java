/**
 * The utils package contains generic utilities
 * Copyright (c) January 2002 F. Esquembre
 * @author F. Esquembre (http://fem.um.es).
 */

package org.colos.ejs.osejs.utils;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import org.colos.ejs.osejs.Osejs;

public class AppSingleDialog {
  static private org.colos.ejs.osejs.utils.ResourceUtil res = new org.colos.ejs.osejs.utils.ResourceUtil("Resources");

  static public enum TEMPLATE { MENU, TABS, SLIDES, CARD };  

  static public class AppSingleOptions {
    private TEMPLATE template = TEMPLATE.MENU;
    private boolean fullScreen = false;
    private boolean simulationFirst = false;
    private ListInformation.LOCKING locking = ListInformation.LOCKING.NONE;
//    boolean noCoverPage = false;

    public TEMPLATE getTemplate() { return template; }
    public boolean isFullScreen() { return fullScreen; }
    public boolean isSimulationFirst() { return simulationFirst; } 
    public ListInformation.LOCKING getScreenLocking() { return locking; }    
  }
  
  static private class ReturnValue {
    boolean value = false;
  }
  
  static public AppSingleOptions getSingleAppOptions (final Osejs ejs, final Component parentComponent) {
    final ReturnValue returnValue=new ReturnValue();
    final JDialog dialog=new JDialog();

    // --------------- Choose template
    
    JLabel templateLabel = new JLabel(res.getString("Package.App.Template"),SwingConstants.LEFT);
//    templateLabel.setFont(templateLabel.getFont().deriveFont(Font.BOLD));

    JRadioButton sideTemplateRB = new JRadioButton (res.getString("Package.App.SideTemplate"),true);
    sideTemplateRB.setRequestFocusEnabled(false);
    JRadioButton tabsTemplateRB = new JRadioButton (res.getString("Package.App.TabsTemplate"),false);
    tabsTemplateRB.setRequestFocusEnabled(false);
    JRadioButton slidesTemplateRB = new JRadioButton (res.getString("Package.App.SlidesTemplate"),false);
    slidesTemplateRB.setRequestFocusEnabled(false);
    JRadioButton cardTemplateRB = new JRadioButton (res.getString("Package.App.CardTemplate"),false);
    cardTemplateRB.setRequestFocusEnabled(false);
    
    ButtonGroup templateGroup = new ButtonGroup();
    templateGroup.add(sideTemplateRB);
    templateGroup.add(tabsTemplateRB);
    templateGroup.add(slidesTemplateRB);
    templateGroup.add(cardTemplateRB);

    JPanel templateOptionsPanel = new JPanel (new GridLayout(2,2)); // FlowLayout(FlowLayout.CENTER)); // GridLayout(1,0));
    templateOptionsPanel.add(sideTemplateRB);
    templateOptionsPanel.add(tabsTemplateRB);
    templateOptionsPanel.add(slidesTemplateRB);
    templateOptionsPanel.add(cardTemplateRB);
    
    JPanel templateCenterPanel = new JPanel (new FlowLayout(FlowLayout.CENTER));
    templateCenterPanel.add(templateOptionsPanel);

    JPanel templatePanel = new JPanel (new BorderLayout());
    templatePanel.setBorder(new EmptyBorder(5,0,0,0));
    templatePanel.add(templateLabel,BorderLayout.NORTH);
    templatePanel.add(templateCenterPanel,BorderLayout.CENTER);

    // --------------- Lock radiobuttons
    
    JLabel lockLabel = new JLabel(res.getString("Package.App.LockingType"),SwingConstants.LEFT);
//    lockLabel.setFont(lockLabel.getFont().deriveFont(Font.BOLD));

    JRadioButton lockPortraitRB = new JRadioButton (res.getString("Package.App.LockPortrait"),false);
    lockPortraitRB.setRequestFocusEnabled(false);
    JRadioButton lockLandscapeRB = new JRadioButton (res.getString("Package.App.LockLandscape"),false);
    lockLandscapeRB.setRequestFocusEnabled(false);
    JRadioButton noLockRB = new JRadioButton (res.getString("Package.App.LockNothing"),true);
    noLockRB.setRequestFocusEnabled(false);

    ButtonGroup lockGroup = new ButtonGroup();
    lockGroup.add(noLockRB);
    lockGroup.add(lockPortraitRB);
    lockGroup.add(lockLandscapeRB);

    FlowLayout lockFlow = new FlowLayout(FlowLayout.CENTER);
    lockFlow.setVgap(0);
    JPanel lockOptionsPanel = new JPanel (lockFlow);
    lockOptionsPanel.add(noLockRB);
    lockOptionsPanel.add(lockPortraitRB);
    lockOptionsPanel.add(lockLandscapeRB);

    JPanel lockPanel = new JPanel (new BorderLayout());
    lockPanel.setBorder(new EmptyBorder(5,0,0,0));
    lockPanel.add (lockLabel,BorderLayout.NORTH);
    lockPanel.add (lockOptionsPanel,BorderLayout.CENTER);    

    // --------------- Features checkboxes
    
    JCheckBox fullScreenCB = new JCheckBox (res.getString("Package.App.FullScreen"),false);
    fullScreenCB.setRequestFocusEnabled(false);

    JCheckBox simulationFirstCB = new JCheckBox (res.getString("Package.App.SimulationFirst"),false);
    simulationFirstCB.setRequestFocusEnabled(false);

    JPanel otherPanel=new JPanel (new java.awt.GridLayout(0,1));
    otherPanel.setBorder(new EmptyBorder(10,5,5,0));
    otherPanel.add(fullScreenCB);
    otherPanel.add(simulationFirstCB);

//    JLabel featuresLabel = new JLabel(res.getString("Package.App.OtherProperties"));

    // --------------------- Main Buttons

    JButton okButton = new JButton (res.getString("EditorFor.Ok"));
    okButton.addActionListener (new ActionListener() {
      public void actionPerformed (java.awt.event.ActionEvent evt) {
        returnValue.value = true;
        dialog.setVisible (false);
      }
    });

    JButton cancelButton = new JButton (res.getString("EditorFor.Cancel"));
    cancelButton.addActionListener (new ActionListener() {
      public void actionPerformed (java.awt.event.ActionEvent evt) {
        returnValue.value =false;
        dialog.setVisible (false);
      }
    });
    JPanel buttonsPanel = new JPanel (new FlowLayout(FlowLayout.CENTER));
    buttonsPanel.add (okButton);
    buttonsPanel.add (cancelButton);

    // ------------- Put everything together ---------------
    
    Box optionsBox = Box.createVerticalBox();
    optionsBox.setBorder(new EmptyBorder(10,10,2,10));
    optionsBox.add(templatePanel);
    optionsBox.add(lockPanel);
    optionsBox.add(otherPanel);
    
    JPanel bottomPanel = new JPanel (new BorderLayout());
    bottomPanel.add(new JSeparator(),BorderLayout.NORTH);
    bottomPanel.add(buttonsPanel,BorderLayout.CENTER);
    
    // Put everything in the dialog
    dialog.getContentPane().setLayout (new BorderLayout(5,0));
    dialog.getContentPane().add (optionsBox,BorderLayout.NORTH);
    dialog.getContentPane().add (bottomPanel,BorderLayout.SOUTH);
    
    dialog.validate();
    dialog.pack();
    dialog.setModal(true);
    dialog.setTitle (res.getString("Package.App.SingleAppTitle"));
    dialog.setLocationRelativeTo (parentComponent);

    returnValue.value = false;
    dialog.setVisible(true);
    if (!returnValue.value) return null;

    AppSingleOptions info = new AppSingleOptions();
    if      (sideTemplateRB.isSelected())    info.template = TEMPLATE.MENU;
    else if (tabsTemplateRB.isSelected())    info.template = TEMPLATE.TABS;
    else if (slidesTemplateRB.isSelected())  info.template = TEMPLATE.SLIDES;
    else                                     info.template = TEMPLATE.CARD;                    
    // checkbox
    info.fullScreen = fullScreenCB.isSelected();
    info.simulationFirst = simulationFirstCB.isSelected();
    // locking
    if (lockPortraitRB.isSelected())        info.locking = ListInformation.LOCKING.PORTRAIT;
    else if (lockLandscapeRB.isSelected())  info.locking = ListInformation.LOCKING.LANDSCAPE;
    else                                    info.locking = ListInformation.LOCKING.NONE;  
    
    return info;
  }

  
}
