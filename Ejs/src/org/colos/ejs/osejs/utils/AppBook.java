package org.colos.ejs.osejs.utils;

import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.colos.ejs.osejs.Osejs;
import org.colos.ejs.osejs.OsejsCommon;
import org.opensourcephysics.tools.JarTool;
import org.opensourcephysics.tools.minijar.PathAndFile;

public class AppBook {
  static private final ResourceUtil res = new ResourceUtil("Resources");
  
  /**
   * Compresses a list of simulations in a single ZIP file
   * @param _ejs
   * @param _listInfo 
   * @param _targetFile
   */
  static public void convertToBookApp(Osejs _ejs, ListInformation _listInfo, File _targetFile) {
    // Temporary folder that will contain all generated files
    File tmpFolder = AppSingle.prepareTargetFolder(_ejs,"javascript/APP_BOOK/IONIC2/");

    // Target Ionic folders
    File ejsModelsFolder = new File(tmpFolder,"src/assets/ejs_pages");
    File physletsModelsFolder = new File(tmpFolder,"src/assets/physlets_pages");
    File permanentModelsFolder = ejsModelsFolder;
    
    boolean ignoreHTML = !_listInfo.getIncludeHTMLFiles();
    boolean separateChapters = _listInfo.getSeparateChapters();

    // Prepare buffers for the processing
    ArrayList<TocEntry> tocList = new ArrayList<TocEntry>();
    boolean hasEJSModel = false;
    boolean hasPhysletModel = false;

    // Uncompress all zip files into the zip directory
    java.util.List<PathAndFile> listOfZipFiles = _listInfo.getList();
    int globalSectionCounter = 1;
    for (int modelCounter=1,n=listOfZipFiles.size(); modelCounter<=n; modelCounter++) {
      PathAndFile paf = listOfZipFiles.get(modelCounter-1);
      String modelName = "_model_"+modelCounter;
      File tmpModelFolder = new File(tmpFolder,modelName);
      tmpModelFolder.mkdirs();
      TwoStrings ts = FileUtils.getPlainNameAndExtension(paf.getFile());
      String zipName = ts.getFirstString();
      JarTool.unzip(paf.getFile(), tmpModelFolder);
      
      AppEJSModel appModel;
      if (AppEJSModel.isEJSModel(tmpModelFolder)) { 
        permanentModelsFolder = ejsModelsFolder;
        appModel = new AppEJSModel();
        if (!hasEJSModel) {
          ejsModelsFolder.mkdirs();
          appModel.addFilesToTargetFolder(_ejs, tmpFolder);
        }
        else hasEJSModel = true;
      }
      else if (AppPhysletModel.isPhysletModel(tmpModelFolder,zipName)) { // Try Physlet model
        permanentModelsFolder = physletsModelsFolder;
        appModel = new AppPhysletModel();
        if (!hasPhysletModel) {
          physletsModelsFolder.mkdirs();
          appModel.addFilesToTargetFolder(_ejs, tmpFolder);
        }
        else hasPhysletModel = true;
      }
      else {
        _ejs.getOutputArea().println("Warning: ZIP file "+paf.getPath()+" does not contain a valid EJS or Physlet model. Ignored!");
        JarTool.remove(tmpModelFolder);
        continue;
      }
      appModel.processModel(tmpFolder, tmpModelFolder, permanentModelsFolder, modelName,zipName,!ignoreHTML);
      JarTool.remove(tmpModelFolder);
      // Create Table of Contents
      TocEntry modelTocEntry=null;
      if (separateChapters) {
        switch(_listInfo.getNumberingOption()) {
          default : 
          case NONE : 
            modelTocEntry = TocEntry.createChapter(appModel.getTitle(),res.getString("SimInfoEditor.Author")+" "+appModel.getAuthor());
            break;
          case CHAPTER : 
          case CHAPTER_AND_SECTION : 
            modelTocEntry = TocEntry.createChapter(modelCounter+". "+appModel.getTitle(),res.getString("SimInfoEditor.Author")+" "+appModel.getAuthor());
            break;
        }
        tocList.add(modelTocEntry);
      }
      // Add section entries
      ArrayList<TwoStrings> modelPages = appModel.getModelPages(); 
      for (int page=1,nPages=modelPages.size(); page<=nPages; page++,globalSectionCounter++) {
        TwoStrings entry = modelPages.get(page-1);
        String textForEntry;
        switch(_listInfo.getNumberingOption()) {
          default : 
          case NONE : 
          case CHAPTER : 
            textForEntry = entry.getFirstString();
            break;
          case CHAPTER_AND_SECTION :
            if (separateChapters) textForEntry = modelCounter+"."+page+" "+entry.getFirstString();
            else textForEntry = globalSectionCounter+". "+entry.getFirstString();
            break;
        }
        if (modelTocEntry==null) tocList.add(TocEntry.createSection(textForEntry,entry.getSecondString()));
        else modelTocEntry.addSection(TocEntry.createSection(textForEntry,entry.getSecondString()));
      }
      
    } // end of for loop for each model

   // Create main file
    String logoFilename = "assets/ejs_img/Gyroscope.png";
    if (_listInfo.getImagefile()!=null) try {
      FileUtils.copy(_listInfo.getImagefile(),new File(tmpFolder,"src/assets/ejs_img/cover.png"));
      logoFilename = "assets/ejs_img/cover.png";
    }
    catch (Exception exc) {
      JOptionPane.showMessageDialog(_ejs.getMainPanel(), 
          res.getString("Osejs.File.SavingError")+"\nsrc/assets/ejs_img/cover.xhtml", 
          res.getString("Warning"), JOptionPane.ERROR_MESSAGE);
    }
    createBookStructureFile(_ejs, tmpFolder, _listInfo, logoFilename, tocList);
    
    // Copy the temporary folder contents to the IONIC project folder
    FileUtils.copyDirectory(tmpFolder, _targetFile,true);
    JarTool.remove(tmpFolder);
    _ejs.getOutputArea().println(res.getString("App.AppModified")+ " : "+_targetFile.getAbsolutePath());
  }

  // -------------------------------
  // Specific book App structure
  // -------------------------------
  
  static public void createBookStructureFile(Osejs _ejs, File _workingFolder, 
      ListInformation _listInfo, String _logoFilename, ArrayList<TocEntry> _tocList) {

   StringBuffer buffer = new StringBuffer();
    buffer.append("var book_title = \""+_listInfo.getName()+"\";\n");
    buffer.append("var book_header = \""+_listInfo.getHeader()+"\";\n");
    buffer.append("var book_author = \""+_listInfo.getAuthor()+"\";\n");
    buffer.append("var book_abstract = \""+_listInfo.getAbstract()+"\";\n");
    buffer.append("var book_copyright = \""+_listInfo.getCopyright()+"\";\n");
    buffer.append("var book_logo = \""+_logoFilename+"\";\n\n");
    
    buffer.append("var book_menu_title = \""+res.getString("Package.Contents")+"\";\n");
    buffer.append("var book_toc_title = \""+res.getString("Package.TableOfContents")+"\";\n");
    buffer.append("var book_copyright_title = \""+res.getString("Package.Copyright")+"\";\n");
    buffer.append("var book_about_title = \""+res.getString("Package.About")+"\";\n\n");

    buffer.append("var book_locking = ");
    switch (_listInfo.getScreenLocking()) {
      case PORTRAIT  : buffer.append("'portrait';\n"); break;
      case LANDSCAPE : buffer.append("'landscape';\n"); break;
      default :
      case NONE : buffer.append("'none';\n"); break;
    }

    buffer.append("var book_toc = [\n");
    buffer.append("  {\n");
    buffer.append("    title: \""+res.getString("Package.TableOfContents")+"\",\n");
    buffer.append("    type: \"toc\"\n");
    buffer.append("  }");
    for (TocEntry entry : _tocList) {
      buffer.append(",\n");
      buffer.append("  {\n");
      buffer.append("    title: \""+entry.title+"\"");
      if (entry.url!=null)         buffer.append(",\n    url:\""+entry.url+"\"");
      if (entry.description!=null) buffer.append(",\n    description:\""+entry.description+"\"");
      if (entry.type==TocEntry.TYPE.CHAPTER) { // Chapter
        buffer.append(",\n    type: \"chapter\",\n");
        buffer.append("    sections : [");
        int sectionCounter = 1;
        for (TocEntry section : entry.sections) {
          if (sectionCounter==1) buffer.append("\n");
          else buffer.append(",\n");
          buffer.append("      {\n");
          buffer.append("        title: \""+section.title+"\"");
          if (section.url!=null)         buffer.append(",\n        url: \""+section.url+"\"");
          if (section.description!=null) buffer.append(",\n        description:\""+section.description+"\"");
          buffer.append("\n      }");
          sectionCounter++;
        }
        buffer.append("\n    ]");
      }
      else {
        buffer.append(",\n    type: \"section\"\n");
      }
      buffer.append("\n  }");
    }
    buffer.append("\n  ];\n");

    File outputFile = new File (_workingFolder,"src/assets/ejs_js/book.js");
    try {
      FileUtils.saveToFile(outputFile, OsejsCommon.getUTF8(), buffer.toString());
    }
    catch (Exception exc) {
      exc.printStackTrace();
      JOptionPane.showMessageDialog(_ejs.getMainPanel(), 
          res.getString("Osejs.File.SavingError")+"\nassets/ejs_js/book.js", 
          res.getString("Warning"), JOptionPane.ERROR_MESSAGE);
    }
  }

  // -------------------------------
  // static inner classes and utils
  // -------------------------------
  
  static private class TocEntry {
    static private enum TYPE { CHAPTER, SECTION };
    
    String title, url, description;
    TYPE type;
    ArrayList<TocEntry> sections;
    
    private TocEntry() {};
    
    static TocEntry createChapter(String _title, String _desc) {
      TocEntry entry = new TocEntry();
      entry.title = _title;
      entry.type = TYPE.CHAPTER;
      entry.description = _desc;
      entry.sections = new ArrayList<TocEntry>();
      return entry;
    }

    static TocEntry createSection(String _title, String _url) {
      TocEntry entry = new TocEntry();
      entry.title = _title;
      entry.type = TYPE.SECTION;
      entry.url = _url;
      return entry;
    }
    
    public boolean addSection(TocEntry _section) {
      if (type == TYPE.CHAPTER) {
        sections.add(_section);
        return true;
      }
      return false;
    }

  };

}
