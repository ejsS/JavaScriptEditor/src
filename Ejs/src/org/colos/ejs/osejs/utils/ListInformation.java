/**
 * The utils package contains generic utilities
 * Copyright (c) August 2018 F. Esquembre
 * @author F. Esquembre (http://fem.um.es).
 */

package org.colos.ejs.osejs.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JComponent;
import javax.swing.JOptionPane;

import org.colos.ejs.osejs.OsejsCommon;
import org.opensourcephysics.tools.minijar.PathAndFile;

public class ListInformation {
  static private ResourceUtil res = new ResourceUtil("Resources");

  static public enum NUMBERING { NONE, CHAPTER, CHAPTER_AND_SECTION };
  static public enum STRUCTURE { NESTED, FLAT, STRICT };
  static public enum LOCKING { NONE, PORTRAIT, LANDSCAPE };

  boolean asFolder=true;
  boolean readOnly=true;
  String name="";
  String header= "";
  String author="";
  String about="";
  String copyright="";

  File imageFile=null;
  File infoFile=null;
  boolean includeHTMLFiles=true;
  boolean separateChapters=false;
  STRUCTURE foldersStructure=STRUCTURE.NESTED;
  NUMBERING numberingOption = NUMBERING.NONE;
  LOCKING lockingOption = LOCKING.NONE;
  java.util.List<PathAndFile> list = new ArrayList<PathAndFile>();

  public boolean isReadOnly() { return readOnly; }
  public String getName() { 
    if (name.trim().length()<=0) return "Untitled"; 
    return name.trim(); 
  }
  public String getHeader() { 
    if (header.trim().length()<=0) return "Untitled"; 
    return header.trim(); 
  }
  public String getAuthor() { 
    if (author.trim().length()<=0) return "Unknown author"; 
    return author.trim(); 
  }
  public String getAbstract() { 
    if (about.trim().length()<=0) return "No information available"; 
    return about.trim(); 
  }
  public String getCopyright() { 
    if (copyright.trim().length()<=0) return "No copyright information available"; 
    return copyright.trim(); 
  }
  public File getImagefile() { return imageFile; }
  public File getInfoFile() { return infoFile; }
  public java.util.List<PathAndFile> getList() { return list; }
  public boolean getIncludeHTMLFiles() { return includeHTMLFiles; }
  public boolean getSeparateChapters() { return separateChapters; }
  public NUMBERING getNumberingOption() { return numberingOption; }
  public STRUCTURE getFoldersStructure() { return foldersStructure; }
  
  public LOCKING getScreenLocking() { return lockingOption; }

  public void saveToFile(File file, JComponent button) {
    StringBuffer buffer = new StringBuffer();
    buffer.append("<?xml version=\"1.0\" encoding=\"UTF-16\"?>\n");
    buffer.append("<EjsSListConfig>\n");
    buffer.append("<AsFolder>"+asFolder+"</AsFolder>\n");
    buffer.append("<ReadOnly>"+readOnly+"</ReadOnly>\n");
    buffer.append("<Name><![CDATA["+name+"]]></Name>\n");
    buffer.append("<Header><![CDATA["+header+"]]></Header>\n");
    buffer.append("<Author><![CDATA["+author+"]]></Author>\n");
    buffer.append("<About><![CDATA["+about+"]]></About>\n");
    buffer.append("<Copyright><![CDATA["+copyright+"]]></Copyright>\n");
    if (imageFile==null) buffer.append("<ImageFile></ImageFile>\n");
    else buffer.append("<ImageFile>"+FileUtils.getRelativePath(imageFile,file.getParentFile(),true)+"</ImageFile>\n");
    if (infoFile==null) buffer.append("<InfoFile></InfoFile>\n");
    else buffer.append("<InfoFile>"+FileUtils.getRelativePath(infoFile,file.getParentFile(),true)+"</InfoFile>\n");
    buffer.append("<IncludeHTML>"+includeHTMLFiles+"</IncludeHTML>\n");
    buffer.append("<ModelsAsChapters>"+separateChapters+"</ModelsAsChapters>\n");
    switch(lockingOption) {
      case PORTRAIT  : buffer.append("<Locking>PORTRAIT</Locking>\n");  break;
      case LANDSCAPE : buffer.append("<Locking>LANDSCAPE</Locking>\n");  break;
      default : 
      case NONE   : buffer.append("<Locking>NONE</Locking>\n");  break;
    }
    switch(foldersStructure) {
      case FLAT :   buffer.append("<FolderStructure>FLAT</FolderStructure>\n"); break;
      case STRICT : buffer.append("<FolderStructure>STRICT</FolderStructure>\n"); break;
      default : 
      case NESTED : buffer.append("<FolderStructure>NESTED</FolderStructure>\n"); break;
    }
    switch(numberingOption) {
      case CHAPTER :             buffer.append("<Numbering>CHAPTER</Numbering>\n");  break;
      case CHAPTER_AND_SECTION : buffer.append("<Numbering>CHAPTER_AND_SECTION</Numbering>\n");  break;
      default : 
      case NONE :                buffer.append("<Numbering>NONE</Numbering>\n");  break;
    }
    buffer.append("<ModelList>\n");
    for (int i=0,n=list.size(); i<n; i++) {
      buffer.append(FileUtils.getRelativePath(list.get(i).getFile(),file.getParentFile(),true)+"\n");
    }
    buffer.append("</ModelList>\n");
    buffer.append("</EjsSListConfig>\n");
    try {
      FileUtils.saveToFile(file, OsejsCommon.getUTF16(), buffer.toString());
    } catch (IOException e) {
      // TODO Auto-generated catch block
      JOptionPane.showMessageDialog(button, 
          res.getString("Osejs.File.SavingError")+"\n"+file.getName(), 
          res.getString("Error"), JOptionPane.ERROR_MESSAGE);
      e.printStackTrace();
    }    
  }

  static private String readEntry (String _input, String _key, boolean cData) {
    try {
      String piece;
      if (cData) piece = OsejsCommon.getPiece(_input,"<"+_key+"><![CDATA[","]]></"+_key+">",false);
      else       piece = OsejsCommon.getPiece(_input,"<"+_key+">","</"+_key+">",false);
      return piece;
    }
    catch (Exception exc) {
      return null;
    }
  }

  public void readFromFile(File file, JComponent component) {
    String input = readEntry(FileUtils.readTextFile(file,OsejsCommon.getUTF16()),"EjsSListConfig",false);
    if (input==null || input.trim().length()<=0) {
      JOptionPane.showMessageDialog(component, 
          res.getString("Osejs.File.InvalidFile")+"\n"+file.getName(), 
          res.getString("Error"), JOptionPane.ERROR_MESSAGE);
      return;
    }
    String piece = readEntry(input,"AsFolder",false);
    if (piece!=null) asFolder = (piece.indexOf("true")>=0);
    piece = readEntry(input,"ReadOnly",false);
    if (piece!=null) readOnly = (piece.indexOf("true")>=0);
    piece = readEntry(input,"Name",true);
    if (piece!=null) name = piece;
    else name = "";
    piece = readEntry(input,"Header",true);
    if (piece!=null) header = piece;
    else header = "";
    piece = readEntry(input,"Author",true);
    if (piece!=null) author = piece;
    else author = "";
    piece = readEntry(input,"About",true);
    if (piece!=null) about = piece;
    else about = "";
    piece = readEntry(input,"Copyright",true);
    if (piece!=null) copyright = piece;
    else copyright = "";
    piece = readEntry(input,"ImageFile",false);
    if (piece!=null && piece.trim().length()>0) imageFile = new File(file.getParentFile(),piece);
    else imageFile = null;
    piece = readEntry(input,"InfoFile",false);
    if (piece!=null && piece.trim().length()>0) infoFile = new File(file.getParentFile(),piece);
    else infoFile = null;
    piece = readEntry(input,"IncludeHTML",false);
    if (piece!=null) includeHTMLFiles = (piece.indexOf("true")>=0);
    piece = readEntry(input,"ModelsAsChapters",false);
    if (piece!=null) separateChapters = (piece.indexOf("true")>=0);
    piece = readEntry(input,"FolderStructure",false);
    if (piece!=null) {
      if (piece.equals("FLAT"))        foldersStructure = STRUCTURE.FLAT;
      else if (piece.equals("STRICT")) foldersStructure = STRUCTURE.STRICT;
      else                             foldersStructure = STRUCTURE.NESTED;
    }
    piece = readEntry(input,"Locking",false);
    if (piece!=null) {
      if (piece.equals("PORTRAIT"))       lockingOption = LOCKING.PORTRAIT;
      else if (piece.equals("LANDSCAPE")) lockingOption = LOCKING.LANDSCAPE;
      else                                lockingOption = LOCKING.NONE;
    }
    
    piece = readEntry(input,"Numbering",false);
    if (piece!=null) {
      if (piece.equals("CHAPTER"))                  numberingOption = NUMBERING.CHAPTER;
      else if (piece.equals("CHAPTER_AND_SECTION")) numberingOption = NUMBERING.CHAPTER_AND_SECTION;
      else                                          numberingOption = NUMBERING.NONE;
    }
    piece = readEntry(input,"ModelList",false);
    if (piece!=null) {
      StringBuffer buffer = new StringBuffer();
      StringTokenizer tkn = new StringTokenizer(piece.trim(),"\n");
      while (tkn.hasMoreTokens()) {
        String model = tkn.nextToken();
        File modelFile = new File(file.getParentFile(),model);
        if (modelFile.exists()) list.add(new PathAndFile(model,modelFile));
        else buffer.append(model+"\n");
      }
      String errorList = buffer.toString();
      if (errorList.length()>0) JOptionPane.showMessageDialog(component, 
          res.getString("SimInfoEditor.RequiredFileNotFound")+"\n"+errorList, 
          res.getString("Warning"), JOptionPane.ERROR_MESSAGE);
    }
  }
  
}
