/**
 * The package contains the main functionality of Osejs
 * Copyright (c) November 2001 F. Esquembre
 * @author F. Esquembre (http://fem.um.es).
 */

package org.colos.ejs.osejs;

import org.colos.ejs._EjsSConstants;
//import org.colos.ejs.library.server.SocketView.Message;
//import org.colos.ejs.library.server.utils.MetadataAPI;
//import org.colos.ejs.library.server.utils.MetadataBuilder;
import org.colos.ejs.library.utils.LocaleItem;
import org.colos.ejs.osejs.utils.*;
import org.colos.ejs.osejs.edition.Editor;
import org.colos.ejs.osejs.edition.html.HtmlEditor;
import org.colos.ejs.osejs.edition.html.OneHtmlPage;
import org.colos.ejss.xml.JSObfuscator;
import org.opensourcephysics.tools.minijar.PathAndFile;
import org.opensourcephysics.display.DisplayRes;
import org.opensourcephysics.display.OSPRuntime;
import org.opensourcephysics.tools.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
//import java.util.HashMap;
//import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
//import java.util.Map;
//import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.*;
//import javax.json.Json;
//import javax.json.JsonObject;
//import javax.json.JsonReader;


//--------------------

public class GenerateUtils {
  static private final ResourceUtil res    = new ResourceUtil("Resources");
  static public final String sVersionInfo = "generated-with: Easy Java/Javascript Simulations";
 

  static public void copyEJSLibrary(Osejs _ejs) {
    // Add all the files in the library directory 
    File binConfigDir = new File (_ejs.getBinDirectory(),OsejsCommon.CONFIG_DIR_PATH);
    String binConfigDirPath = FileUtils.getPath(binConfigDir);
    for (File file : JarTool.getContents(new File(binConfigDir,OsejsCommon.EJS_LIBRARY_DIR_PATH))) {
      String destName = FileUtils.getRelativePath(file, binConfigDirPath, false);
      //      System.err.println("Copying "+file +" to "+new File (_ejs.getOutputDirectory(),destName));
      JarTool.copy(file, new File (_ejs.getOutputDirectory(),destName));
    }

    // Overwrite files in the library directory with user defined files (if any)
    String configDirPath = FileUtils.getPath(_ejs.getConfigDirectory());
    for (File file : JarTool.getContents(new File(_ejs.getConfigDirectory(),OsejsCommon.EJS_LIBRARY_DIR_PATH+"/css"))) {
      String destName = FileUtils.getRelativePath(file, configDirPath, false);
      //      System.err.println("Copying 2 "+file +" to "+new File (_ejs.getOutputDirectory(),destName));
      JarTool.copy(file, new File (_ejs.getOutputDirectory(),destName));
    }
  }


  
  // ----------------------------
  // Options of the package button
  // ----------------------------

  /**
   * Compresses a list of simulations in a single ZIP file
   * @param _ejs
   * @param _listInfo 
   * @param _targetFile
   */
  static private void packageSeveralXMLSimulations(Osejs _ejs, ListInformation _listInfo, File _targetFile) {
    _ejs.getExportDirectory().mkdirs(); // In case it doesn't exist
    //System.out.println ("packaging "+_targetFile.getAbsolutePath());
    _ejs.getOutputArea().message("Package.PackagingJarFile",_targetFile.getName());

    // Create a working temporary directory
    File zipFolder=null;
    try {
      zipFolder = File.createTempFile("EjsPackage", ".tmp", _ejs.getExportDirectory()); // Get a unique name for our temporary directory
      zipFolder.delete();        // remove the created file
      zipFolder.mkdirs();
    } catch (Exception exc) { 
      exc.printStackTrace();
      JOptionPane.showMessageDialog(_ejs.getMainPanel(),res.getString("Package.JarFileNotCreated"),
          res.getString("Osejs.File.Error"), JOptionPane.INFORMATION_MESSAGE);
      JarTool.remove(zipFolder);
      return;
    }

    String ret = System.getProperty("line.separator");
    StringBuffer metadataBuffer = new StringBuffer();
    metadataBuffer.append("package:true"+ret);
    metadataBuffer.append(sVersionInfo+ ", version "+_EjsSConstants.VERSION+", build "+_EjsSConstants.VERSION_DATE+". Visit "+_EjsSConstants.WEB_SITE+ret);
    String title = "";
    java.util.List<PathAndFile> listOfZipFiles = _listInfo.getList();
    boolean isFolder = _listInfo.getName()!=null;

    if (isFolder) { // It is a directory
      String name = _listInfo.getName().trim();
      if (name.length()<=0) name = "Unnamed";
      metadataBuffer.append("folder:"+name+ret);
      metadataBuffer.append("read-only:"+_listInfo.isReadOnly()+ret);
      String author = _listInfo.getAuthor();
      if (author.length()>0) metadataBuffer.append("author:"+author+ret);
      if (_listInfo.getImagefile()!=null) {
        File iconFile = _listInfo.getImagefile();
        try {
          if (FileUtils.copy(new FileInputStream(iconFile), new File(zipFolder,"_folderIcon.png"))) {
            metadataBuffer.append("logo-image: _folderIcon.png"+ret);
          }
          else { 
            JOptionPane.showMessageDialog(_ejs.getMainPanel(), 
                res.getString("Osejs.File.SavingError")+"\n"+iconFile, 
                res.getString("Warning"), JOptionPane.ERROR_MESSAGE);
          }
        } catch (Exception e) {
          JOptionPane.showMessageDialog(_ejs.getMainPanel(), 
              res.getString("Osejs.File.SavingError")+"\n"+iconFile, 
              res.getString("Warning"), JOptionPane.ERROR_MESSAGE);
          e.printStackTrace();
        }
      }
      if (_listInfo.getInfoFile()!=null) {
        File infoFile = _listInfo.getInfoFile();
        try {
          if (FileUtils.copy(new FileInputStream(infoFile), new File(zipFolder,"_infoIcon.png"))) {
            metadataBuffer.append("author-image: _infoIcon.png"+ret);
          }
          else { 
            JOptionPane.showMessageDialog(_ejs.getMainPanel(), 
                res.getString("Osejs.File.SavingError")+"\n"+infoFile, 
                res.getString("Warning"), JOptionPane.ERROR_MESSAGE);
          }
        } catch (Exception e) {
          JOptionPane.showMessageDialog(_ejs.getMainPanel(), 
              res.getString("Osejs.File.SavingError")+"\n"+infoFile, 
              res.getString("Warning"), JOptionPane.ERROR_MESSAGE);
          e.printStackTrace();
        }
      }
    }
    else { // Not a folder. Process first model to obtain metadata
      PathAndFile firstPAF = listOfZipFiles.get(0);

      File firstMetadataFile = JarTool.extract(firstPAF.getFile(), "_metadata.txt", new File(zipFolder,"_metadata.txt"));
      if (firstMetadataFile==null) {
        _ejs.getOutputArea().println("Error: First ZIP file in list: "+firstPAF.getPath()+" does not contain an EjsS Javascript model. Interrupted!");
        JOptionPane.showMessageDialog(_ejs.getMainPanel(),res.getString("Package.JarFileNotCreated"),
            res.getString("Osejs.File.Error"), JOptionPane.INFORMATION_MESSAGE);
        JarTool.remove(zipFolder);
        return;
      }

      String metadata = FileUtils.readTextFile(firstMetadataFile, null);
      StringTokenizer tkn = new StringTokenizer(metadata,"\n");
      while (tkn.hasMoreTokens()) {
        String line = tkn.nextToken();
        if (line.startsWith("title:")) {
          title = line.substring(6).trim();
          metadataBuffer.append("title:"       +title+ret);
        }
        else if (line.startsWith("logo-image:")) {
          String logoStr = line.substring(11).trim();
          metadataBuffer.append("logo-image:model1/"+ logoStr + ret);
        }
        else if (line.startsWith("author:")) {
          metadataBuffer.append("author:" +line.substring(7).trim()+ret);
        }
        else if (line.startsWith("copyright:")) {
          metadataBuffer.append("copyright:"   +line.substring(10).trim()+ret);
        }
        else if (line.startsWith("author-image:")) {
          String imgStr = line.substring(13).trim();
          metadataBuffer.append("author-image:");
          StringTokenizer imgTkn = new StringTokenizer(imgStr,";",true);
          while (imgTkn.hasMoreTokens()) {
            String oneImgStr = imgTkn.nextToken();
            if (oneImgStr.equals(";")) metadataBuffer.append(";");
            else if (oneImgStr.startsWith("./")) metadataBuffer.append("model1/"+oneImgStr.substring(2));
            else metadataBuffer.append("model1/"+oneImgStr);
          }
          metadataBuffer.append(ret);
        }
      }
      firstMetadataFile.delete();
    }

    StringBuffer htmlBuffer = new StringBuffer();
    if (!isFolder) { 
      // --- BEGIN OF the HTML page for the table of contents
      if (JSObfuscator.isGenerateXHTML()) {
        htmlBuffer.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>"+ret);
        htmlBuffer.append("<!DOCTYPE html>"+ret);
        htmlBuffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:epub=\"http://www.idpf.org/2007/ops\">"+ret);
      }
      else htmlBuffer.append("<html>"+ret);
      htmlBuffer.append("  <head>"+ret);
      htmlBuffer.append("    <title>Contents</title>"+ret);
      htmlBuffer.append("    <base target=\"_self\" />" + ret);
      htmlBuffer.append("    <link rel=\"stylesheet\" type=\"text/css\" href=\"model1/_ejs_library/css/ejsContentsLeft.css\"></link>" + ret);
      htmlBuffer.append("  </head>"+ret);
      String bodyOptions = _ejs.getOptions().getHtmlBody();
      if (bodyOptions.length()>0) htmlBuffer.append("  <body "+bodyOptions+"> "+ret);
      else htmlBuffer.append("  <body> "+ret);
      htmlBuffer.append("    <h1>"+title+"</h1>"+ret);
      htmlBuffer.append("    <h2>" + res.getString("Generate.HtmlContents") + "</h2>"+ret);
      htmlBuffer.append("    <div class=\"contents\">"+ret);
    }

    // Uncompress all zip files into the zip directory
    int counter = 1;
    String firstHtmlPage = null;
    for (int i=0,n=listOfZipFiles.size(); i<n; i++) {
      PathAndFile paf = listOfZipFiles.get(i);
      String targetFolderName = "model"+counter;
      File targetFolder = new File(zipFolder,targetFolderName);
      targetFolder.mkdirs();
      JarTool.unzip(paf.getFile(), targetFolder);
      File metadataFile = new File(targetFolder,"_metadata.txt");
      if (!metadataFile.exists()) { // This is not an ejss_model file
        _ejs.getOutputArea().println("Warning: ZIP file "+paf.getPath()+" does not contain an EJS Javascript model. Ignored!");
        JarTool.remove(targetFolder);
      }
      else { // process metadata file
        String lastPageTitle = "";
        String modelTitle = null;
        String metadata = FileUtils.readTextFile(metadataFile, null);
        StringTokenizer tkn = new StringTokenizer(metadata,"\n");
        while (tkn.hasMoreTokens()) {
          String line = tkn.nextToken();
          if (line.startsWith("title:")) {
            modelTitle = line.substring(6).trim();
          }
          if (line.startsWith("page-title:")) {
            lastPageTitle = line.substring(11).trim();
          }
          else if (line.startsWith("page-index:")) {
            if (!isFolder) {
              String pageIndex = line.substring(11).trim();
              if (firstHtmlPage==null) firstHtmlPage = targetFolderName+"/"+ pageIndex;
              if (pageIndex.endsWith("_Simulation.html") || pageIndex.endsWith("_Simulation.xhtml")) {
                htmlBuffer.append("      <div class=\"simulation\"><a href=\""+ targetFolderName+"/"+ pageIndex + "\" target=\"central\">"+ lastPageTitle +"</a></div>"+ret);
              }
              else { 
                htmlBuffer.append("      <div class=\"intro\"><a href=\""+ targetFolderName+"/"+ pageIndex +"\" target=\"central\">" + lastPageTitle +"</a></div>"+ret);
              }
            }
          }
        }
        if (modelTitle!=null) metadataBuffer.append("model:model"+counter+ " | "+modelTitle +ret);
        else metadataBuffer.append("model:model"+counter+ret);
        counter++;
      }
    }

    if (isFolder) {
      try {
        FileUtils.saveToFile(new File(zipFolder,"_metadata.txt"), null, metadataBuffer.toString());
      } catch (IOException e) {
        _ejs.getOutputArea().println(res.getString("Generate.JarFileNotCreated")+ " : "+_targetFile.getName());
        e.printStackTrace();
        JarTool.remove(zipFolder);
        return;
      }
    }
    else {
      htmlBuffer.append("    </div>"+ret); // End of contents
      // ---- Now the logo
      htmlBuffer.append("    <div class=\"signature\">"+ res.getString("Generate.HtmlEjsGenerated") + " "
          + "<a href=\"http://www.um.es/fem/EjsWiki\" target=\"_blank\">Easy Java Simulations</a></div>"+ret);
      htmlBuffer.append("  </body>"+ret);
      htmlBuffer.append("</html>"+ret);

      // Create main html page
      StringBuffer mainHtmlBuffer = new StringBuffer();

      if (JSObfuscator.isGenerateXHTML()) {
        mainHtmlBuffer.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>"+ret);
        mainHtmlBuffer.append("<!DOCTYPE html>"+ret);
        mainHtmlBuffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:epub=\"http://www.idpf.org/2007/ops\">"+ret);
      }
      else mainHtmlBuffer.append("<html>"+ret);
      mainHtmlBuffer.append("  <head>"+ret);
      mainHtmlBuffer.append("    <title> " + res.getString("Generate.HtmlFor") + " " + title + "</title>"+ret);
      mainHtmlBuffer.append("  </head>"+ret);
      mainHtmlBuffer.append("  <body>"+ret);
      mainHtmlBuffer.append("    <frameset cols=\"25%,*\">"+ret);
      mainHtmlBuffer.append("      <frame src=\"Contents.html\" name=\"contents\" scrolling=\"auto\" target=\"_self\">"+ret);
      if (firstHtmlPage!=null) mainHtmlBuffer.append("      <frame src=\""+firstHtmlPage+"\"");
      else mainHtmlBuffer.append("      <frame src=\"Contents.html\"");
      mainHtmlBuffer.append(" name=\"central\" scrolling=\"auto\" target=\"_self\">"+ret);
      mainHtmlBuffer.append("      <noframes>"+ret);
      mainHtmlBuffer.append("        Gee! Your browser is really old and doesn't support frames. You better update!!!"+ret);
      mainHtmlBuffer.append("      </noframes>"+ret);
      mainHtmlBuffer.append("    </frameset> "+ret);
      mainHtmlBuffer.append("  </body>"+ret);
      mainHtmlBuffer.append("</html>"+ret);

      try {
        FileUtils.saveToFile(new File(zipFolder,"_metadata.txt"), null, metadataBuffer.toString());
        FileUtils.saveToFile(new File(zipFolder,JSObfuscator.isGenerateXHTML() ? "Contexts.xhtml" : "Contents.html"), null, htmlBuffer.toString());
        FileUtils.saveToFile(new File(zipFolder, JSObfuscator.isGenerateXHTML() ? "index.xhtml" : "index.html"), null, mainHtmlBuffer.toString());
      } catch (IOException e) {
        _ejs.getOutputArea().println(res.getString("Generate.JarFileNotCreated")+ " : "+_targetFile.getName());
        e.printStackTrace();
        JarTool.remove(zipFolder);
        return;
      }
    }
    { // save a README file
      StringBuffer buffer = new StringBuffer();
      addToReadMe(buffer,true);
//      for (TwoStrings ts : metaFile) {
//        buffer.append(ts.getFirstString()+": "+ts.getSecondString()+"\n");
//      }
      try {
        FileUtils.saveToFile (new File (zipFolder,"_ejs_README.txt"), null, buffer.toString());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    // Compress and remove working folder
    JarTool.compress(zipFolder, _targetFile, null);
    JarTool.remove(zipFolder);
    // Done
    _ejs.getOutputArea().println(res.getString("Generate.JarFileCreated")+ " : "+_targetFile.getName());
  }

  

  
  static void removeFromPafList (Set<PathAndFile> list, String path) {
    for (PathAndFile paf : list) { 
      if (paf.getPath().equals(path)) {
        list.remove(paf);
        return;
      }
    }
  }
  
  static void addToMetafile(ArrayList<TwoStrings> list, String key, String filepath) {
    if (OSPRuntime.isMac() && filepath.endsWith(".DS_Store")) return;
    if (!filepath.startsWith("/")) {
      if (filepath.startsWith("./")) filepath = filepath.substring(2);
    }
    for (TwoStrings ts : list) {
      if (ts.getFirstString().equals(key) && ts.getSecondString().equals(filepath)) return;
    }
    list.add(new TwoStrings(key,filepath));
  }

  static public void addToReadMe(StringBuffer buffer, boolean multiPackage) {
    buffer.append("<"+res.getString("Osejs.File.FileVersion")+" "+_EjsSConstants.VERSION+" (build "+_EjsSConstants.VERSION_DATE+")>\n\n");
    buffer.append(res.getString("Osejs.Generate.README_1")+" ");
    if (multiPackage) buffer.append(res.getString("Osejs.Generate.README_PACKAGE_CONTAINS"));
    else              buffer.append(res.getString("Osejs.Generate.README_SINGLE_CONTAINS"));
    buffer.append(res.getString("Osejs.Generate.README_2"));
    if (multiPackage) buffer.append(res.getString("Osejs.Generate.README_PACKAGE_STRUCTURE"));
    buffer.append(res.getString("Osejs.Generate.README_HOWTOOPEN"));
    buffer.append(res.getString("Osejs.Generate.README_METADATA"));
    buffer.append(res.getString("Osejs.Generate.README_READER_INFO"));
  }

  /**
   * This compresses the simulation XML file and its auxiliary files
   * @param _ejs Osejs
   * @param _targetFile File
   */
  static public void zipCurrentSimulation(Osejs _ejs, File targetFile) {
    _ejs.getExportDirectory().mkdirs(); // In case it doesn't exist

    File xmlFile = _ejs.getCurrentXMLFile();
    if (!xmlFile.exists()) {
      JOptionPane.showMessageDialog(_ejs.getMainPanel(), res.getString("Package.NoSimulations"),
          res.getString("Osejs.File.Error"), JOptionPane.INFORMATION_MESSAGE);
      return;
    }

    _ejs.getOutputArea().message("Package.PackagingJarFile",targetFile.getName());

    Set<PathAndFile> list = new HashSet<PathAndFile>();
    list.add(new PathAndFile(xmlFile.getName(),xmlFile));

    Set<String> missingAuxFiles = new HashSet<String>();
    Set<String> absoluteAuxFiles = new HashSet<String>();
    for (PathAndFile paf : _ejs.getSimInfoEditor().getAuxiliaryPathAndFiles(false)) {
      if (!paf.getFile().exists()) missingAuxFiles.add(paf.getPath());
      else if (paf.getFile().isDirectory()) { // It is a complete directory
        String prefix = paf.getPath();
        if (!prefix.endsWith("/")) prefix += "/";
        if (prefix.startsWith("./")) prefix = prefix.substring(2);
        else absoluteAuxFiles.add(prefix);
        for (File file : JarTool.getContents(paf.getFile())) {
          list.add(new PathAndFile(prefix+FileUtils.getRelativePath(file,paf.getFile(),false),file));
        }
      }
      else {
        if (paf.getPath().startsWith("./")) list.add(new PathAndFile(paf.getPath().substring(2),paf.getFile()));
        else {
          absoluteAuxFiles.add(paf.getPath());
          list.add(paf);
        }
      }
    }

    //    for (PathAndFile paf : list) System.out.println("Will zip: "+paf.getPath()+" : "+paf.getFile());

    if (org.opensourcephysics.tools.minijar.MiniJar.compress(list, targetFile, null)) {
      _ejs.getOutputArea().println(res.getString("Package.JarFileCreated")+" "+targetFile.getName());
    }
    else _ejs.getOutputArea().println(res.getString("Package.JarFileNotCreated"));
    OsejsCommon.warnAboutFiles(_ejs.getMainPanel(),missingAuxFiles,"SimInfoEditor.RequiredFileNotFound");
    OsejsCommon.warnAboutFiles(_ejs.getMainPanel(),absoluteAuxFiles,"Generate.AbsolutePathsFound");
  }

  /**
   * This compresses several simulation XML files and their auxiliary files
   * @param _ejs Osejs
   */
  static public void zipSeveralSimulations(Osejs _ejs) {
    // Select simulation files or directories to ZIP 
    JFileChooser chooser=OSPRuntime.createChooser(res.getString("View.FileDescription.xmlfile"), new String[]{"xml","ejs", "ejss", "ejsh"},_ejs.getSourceDirectory().getParentFile());
    org.colos.ejs.library.utils.FileUtils.updateComponentTreeUI(chooser);
    chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
    chooser.setMultiSelectionEnabled(true);
    chooser.setCurrentDirectory(_ejs.getFileDialog(null).getCurrentDirectory());
    if (chooser.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) return;
    final File[] dirs = chooser.getSelectedFiles();
    if (dirs==null || dirs.length<=0) {
      System.out.println (res.getString("ProcessCanceled"));
      return;
    }

    File baseDir = chooser.getCurrentDirectory();
    java.util.List<PathAndFile> list = new ArrayList<PathAndFile>();
    for (int i=0,n=dirs.length; i<n; i++) {
      File file = dirs[i];
      if (file.isDirectory()) {
        for (File subFile : JarTool.getContents(file)) 
          if (OsejsCommon.isEJSfile(subFile)) list.add(new PathAndFile(FileUtils.getRelativePath (subFile, baseDir, false),subFile));
      }
      else {
        if (OsejsCommon.isEJSfile(file)) list.add(new PathAndFile(FileUtils.getRelativePath (file, baseDir, false),file));
      }
    }

    java.util.List<?> confirmedList = EjsTool.ejsConfirmList(null,res.getDimension("Package.ConfirmList.Size"),
        res.getString("Package.CompressList"),res.getString("Package.CompressTitle"),list,null);
    if (confirmedList==null || confirmedList.isEmpty()) return;

    // Choose target
    _ejs.getExportDirectory().mkdirs(); // In case it doesn't exist

    File targetFile;
    switch (_ejs.getProgrammingLanguage()) {
      default :
      case JAVA           : targetFile = new File(_ejs.getExportDirectory(),"ejs_src_simulations.zip"); break;
      case JAVASCRIPT     : targetFile = new File(_ejs.getExportDirectory(),"ejss_src_simulations.zip"); break;
      case JAVA_PLUS_HTML : targetFile = new File(_ejs.getExportDirectory(),"ejsh_src_simulations.zip"); break;
    }
    String targetName = FileChooserUtil.chooseFilename(_ejs, targetFile, "ZIP", new String[]{"zip"}, true);
    if (targetName==null) return;
    boolean warnBeforeOverwritting = true;
    if (! targetName.toLowerCase().endsWith(".zip")) targetName = targetName + ".zip";
    else warnBeforeOverwritting = false; // the chooser already checked if the target file exists
    targetFile = new File(targetName);
    if (warnBeforeOverwritting && targetFile.exists()) {
      int selected = JOptionPane.showConfirmDialog(_ejs.getMainPanel(),DisplayRes.getString("DrawingFrame.ReplaceExisting_message") + " " +
          targetFile.getName() +DisplayRes.getString("DrawingFrame.QuestionMark"),
          DisplayRes.getString("DrawingFrame.ReplaceFile_option_title"), JOptionPane.YES_NO_CANCEL_OPTION);
      if (selected != JOptionPane.YES_OPTION) return;
    }

    // Now, do it
    _ejs.getOutputArea().message("Package.PackagingJarFile",targetFile.getName());

    Set<String> missingAuxFiles = new HashSet<String>();
    Set<String> absoluteAuxFiles = new HashSet<String>();
    Set<PathAndFile> packingList = new HashSet<PathAndFile>();
    for (Object xmlObject : confirmedList) {
      PathAndFile xmlPaf = (PathAndFile) xmlObject;
      packingList.add(xmlPaf);
      String xmlName = xmlPaf.getFile().getName();
      String xmlPrefix = xmlPaf.getPath().substring(0,xmlPaf.getPath().indexOf(xmlName));
      for (PathAndFile paf : OsejsCommon.getAuxiliaryFiles(xmlPaf.getFile(),_ejs.getSourceDirectory(),_ejs.getMainPanel())) {
        if (!paf.getFile().exists()) missingAuxFiles.add(paf.getPath());
        else if (paf.getFile().isDirectory()) { // It is a complete directory
          String prefix = paf.getPath();
          if (prefix.startsWith("./")) prefix = xmlPrefix + prefix.substring(2);
          else absoluteAuxFiles.add(prefix);
          for (File file : JarTool.getContents(paf.getFile())) {
            packingList.add(new PathAndFile(prefix+FileUtils.getRelativePath(file,paf.getFile(),false),file));
          }
        }
        else {
          if (paf.getPath().startsWith("./")) packingList.add(new PathAndFile(xmlPrefix+paf.getPath().substring(2),paf.getFile()));
          else {
            absoluteAuxFiles.add(paf.getPath());
            packingList.add(paf);
          }
        }
      }
    }

    //    for (PathAndFile paf : packingList) System.out.println ("Will pack: "+paf.getPath());

    if (org.opensourcephysics.tools.minijar.MiniJar.compress(packingList, targetFile, null)) {
      _ejs.getOutputArea().println(res.getString("Package.JarFileCreated")+" "+targetFile.getName());
    }
    else _ejs.getOutputArea().println(res.getString("Package.JarFileNotCreated"));
    OsejsCommon.warnAboutFiles(_ejs.getMainPanel(),missingAuxFiles,"SimInfoEditor.RequiredFileNotFound");
    OsejsCommon.warnAboutFiles(_ejs.getMainPanel(),absoluteAuxFiles,"Generate.AbsolutePathsFound");
  }


  static public void cleanSimulations(Osejs _ejs) {
    java.util.List<PathAndFile> list = OsejsCommon.getSimulationsMetadataFiles(_ejs.getMainPanel(),_ejs.getOutputDirectory(),
        res.getDimension("Package.ConfirmList.Size"), res.getString("Package.CleanSimulationsMessage"),
        res.getString("Package.CleanSimulations"),true,false);
    if (list==null || list.size()<=0) return;
    boolean result = true;
    for (PathAndFile paf : list) {
      Metadata metadata = Metadata.readFile(paf.getFile(), null);
      // Clean all files created during the compilation process
      for (String filename : metadata.getFilesCreated()) {
        File file = new File (paf.getFile().getParentFile(),filename);
        if (!file.delete()) {
          JOptionPane.showMessageDialog(_ejs.getMainPanel(), 
              res.getString("Package.CouldNotDeleteDir")+" "+FileUtils.getPath(file),
              res.getString("Package.Error"), JOptionPane.INFORMATION_MESSAGE);
          result = false;
        }
      }
    }
    if (result) {
      if (_ejs.getOutputDirectory().exists()) FileUtils.removeEmptyDirs(_ejs.getOutputDirectory(),false); // false = Do not remove the "output" directory itself.
      _ejs.getOutputArea().println(res.getString("Package.SimulationsDeleted"));
    }
    else _ejs.getOutputArea().println(res.getString("Package.SimulationsNotDeleted"));
  }

  static public void packageSeveralXMLSimulations(Osejs _ejs, CreateListDialog.LIST_TYPE listType) {
    ListInformation listInfo = CreateListDialog.createZIPFileList(_ejs, _ejs.getMainFrame(), listType);
    if (listInfo.getList().isEmpty()) return;

    // Choose target
    _ejs.getExportDirectory().mkdirs(); // In case it doesn't exist
    
    File targetFile;
    String targetName;
    boolean warnBeforeOverwritting = true;
    
    switch(listType) {
      case EPUB : 
        targetFile = new File(_ejs.getExportDirectory(), "ejss_model_package.epub");
        targetName = FileChooserUtil.chooseFilename(_ejs, targetFile, "ePub", new String[]{"epub"}, true);
        if (targetName==null) return;
        if (! targetName.toLowerCase().endsWith(".epub")) targetName = targetName + ".epub";
        else warnBeforeOverwritting = false; // the chooser already checked if the target file exists
        break;
      default :
      case BOOK_APP : 
        targetName = "ejss_XXX_BookApp.none";
        targetFile = new File(_ejs.getExportDirectory(), targetName);
        targetName = FileChooserUtil.chooseFoldername(_ejs, targetFile, "", new String[]{""}, false);
        if (targetName==null) return;

        targetFile = new File(targetName);
        if (!targetFile.exists() || !targetFile.isDirectory() || ! new File(targetFile,"src/app").exists()) {
          int selected = JOptionPane.showConfirmDialog(_ejs.getMainPanel(),
              res.getString("App.NotAnApp")+"\n"+
              res.getString("SingleApp.WantHelp"),
              res.getString("Information"), JOptionPane.YES_NO_OPTION);
          if (selected == JOptionPane.YES_OPTION) {
//            String localPage = "file:///"+FileUtils.correctUrlString(FileUtils.getPath(_ejs.getBinDirectory())+
//                "/javascript/SINGLE_APP/HowToCreateAnApp.xhtml");
            String urlPage = "http://www.um.es/fem/EjsWiki/Main/CreateBookApps";
            org.opensourcephysics.desktop.OSPDesktop.displayURL(urlPage);
          }
          return;
        }
        _ejs.getOutputArea().message("App.ModifyingApp",targetFile.getAbsolutePath());
        AppBook.convertToBookApp(_ejs,listInfo,targetFile);
        return;
      case OTHER    :
        targetFile = new File(_ejs.getExportDirectory(), listType==CreateListDialog.LIST_TYPE.OTHER ? "ejss_model_package.zip" : "ejss_bookapp.zip");
        targetName = FileChooserUtil.chooseFilename(_ejs, targetFile, "ZIP", new String[]{"zip"}, true);
        if (targetName==null) return;
        if (! targetName.toLowerCase().endsWith(".zip")) targetName = targetName + ".zip";
        else warnBeforeOverwritting = false; // the chooser already checked if the target file exists
        break;
    }
    targetFile = new File(targetName);
    if (warnBeforeOverwritting && targetFile.exists()) {
      int selected = JOptionPane.showConfirmDialog(_ejs.getMainPanel(),DisplayRes.getString("DrawingFrame.ReplaceExisting_message") + " " +
          targetFile.getName() +DisplayRes.getString("DrawingFrame.QuestionMark"),
          DisplayRes.getString("DrawingFrame.ReplaceFile_option_title"), JOptionPane.YES_NO_CANCEL_OPTION);
      if (selected != JOptionPane.YES_OPTION) return;
    }
    // do it
    switch(listType) {
      case EPUB     : EPub.epubSeveralXMLSimulations(_ejs,listInfo,targetFile); break;
//      case BOOK_APP : BookApp.packageSeveralXMLSimulations(_ejs,listInfo,targetFile); break; 
      default :
      case OTHER    : packageSeveralXMLSimulations(_ejs,listInfo,targetFile); break;
    }
  }


  // ----------------------------
  // Utilities
  // ----------------------------


  /**
   * Converts a collection of filenames into a list of MyPathAndFile
   * @param _ejs
   * @param _xmlFile
   * @param _relativeParent
   * @param _pathList
   * @return
   */
  static public Set<PathAndFile> getPathAndFile (Osejs _ejs, File _parentDir, String _relativeParent, Collection<String> _pathList) {
    Set<PathAndFile> list = new HashSet<PathAndFile>();
    for (String path : _pathList) {
      String fullPath = path;
      File file;
      if (fullPath.startsWith("./")) {
        file = new File (_parentDir,fullPath); // Search relatively to the xmlFile location
        fullPath = _relativeParent + fullPath.substring(2); // complete the relative path 
      }
      else file = new File (_ejs.getSourceDirectory(),fullPath); // Search absolutely in EJS user directory
      if (file.exists()) list.add(new PathAndFile(fullPath,file));
      else _ejs.getOutputArea().println(res.getString("Generate.JarFileResourceNotFound")+": "+path);
    }
    return list;
  }


  // ----------------------------------------------------
  // Generation of HTML code
  // ----------------------------------------------------

//  static private class SimInfo {
//    String name;
//	String fullPath;
//	String path;
//	String classpath;
//	String jarPath;  
//  }

  /**
   * Returns the list of locales desired
   * @param _ejs
   * @return
   */
  static java.util.List<LocaleItem> getLocalesDesired (Osejs _ejs) {
    if (_ejs.getSimInfoEditor().addTranslatorTool()) return _ejs.getTranslationEditor().getDesiredTranslations();
    java.util.List<LocaleItem> localesDesired = new java.util.ArrayList<LocaleItem>();
    localesDesired.add(LocaleItem.getDefaultItem());
    return localesDesired;
  }
  
//  static private String generateMetadataStructure(Osejs _ejs){
//    StringBuffer code = new StringBuffer();
//    MetadataBuilder fullMetadata = new MetadataBuilder("","","");
//    String binDirPath = FileUtils.getPath(_ejs.getBinDirectory());
//    JsonReader jsonMaker;
//    try {
//      InputStream fis = new FileInputStream(binDirPath + "/javascript/API_MODELS_Metadata.json");
//      jsonMaker = Json.createReader(fis);
//      JsonObject objectMaker = jsonMaker.readObject();
//      jsonMaker.close();
//      fullMetadata.load(objectMaker);
//      code.append(fullMetadata.getJSON());
//    } catch (FileNotFoundException e) {
//      // TODO Auto-generated catch block
//      e.printStackTrace();
//    }
//    return code.toString();
//  }
  
  
  /**
   * Adds the set of HTML files required for a frame access to the simulation description pages.
   * The key in the table is the name of the HTML file. The calling method must process these files.
   * @param _htmlTable Hashtable<String,StringBuffer> The table of HTML files
   * @param _ejs Osejs The calling Ejs
   * @param _simulationName String The name of the simulation
   * @param _filename String The base name for the HTML files (different from _simulationName if generating in a server)
   * @param _classpath String The classpath required to run the simulation.
   * @return Hashtable<String,String>
   * @throws IOException
   */
//  static private void addFramesHtml (Hashtable<String,StringBuffer> _htmlTable, Osejs _ejs,
//      String _simulationName, String _javaName, String _packageName, // _javaName is null for pure javascript pages 
//      String _pathToLib, String _archiveStr, boolean hasView) {
//    for (LocaleItem item : getLocalesDesired(_ejs)) addFramesHtml(_htmlTable, _ejs, item,_simulationName, _javaName, _packageName, _pathToLib, _archiveStr, hasView);
//  }

  static void addFramesHtml (Hashtable<String,StringBuffer> _htmlTable, Osejs _ejs, LocaleItem _localeItem,
      String _simulationName, String _javaName, String _packageName, // _javaName is null for pure javascript pages 
      String _pathToLib, String _archiveStr, boolean hasView) {
    String ret = System.getProperty("line.separator");
    boolean left = true;  // Whether to place the content frame at the left frame (true) or at the top frame (false)
    if (_ejs.getOptions().generateHtml()==EjsOptions.GENERATE_TOP_FRAME) left = false;

    //    System.out.println("Generate "+_ejs.getOptions().generateHtml()+ " left = "+left);

    String localeSuffix = _localeItem.isDefaultItem() ? "" : "_"+_localeItem.getKeyword();
    String fileSuffix = JSObfuscator.isGenerateXHTML() ? ".xhtml" : ".html";

    // --- BEGIN OF the HTML page for the table of contents
    StringBuffer code = new StringBuffer();
    if (JSObfuscator.isGenerateXHTML()) {
      code.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>"+ret);
      code.append("<!DOCTYPE html>"+ret);
      code.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:epub=\"http://www.idpf.org/2007/ops\">"+ret);
    }
    else {
      code.append("<html>"+ret);
    }
    code.append("  <head>"+ret);
    code.append("    <title>Contents</title>"+ret);
    code.append("    <base target=\"_self\" />" + ret);
    if (left) {
      code.append("    <link rel=\"stylesheet\" type=\"text/css\" href=\""+_pathToLib+"_ejs_library/css/ejsContentsLeft.css\"></link>" + ret);
    }
    else {
      code.append("    <link rel=\"stylesheet\" type=\"text/css\" href=\""+_pathToLib+"_ejs_library/css/ejsContentsTop.css\"></link>" + ret);
    }
    code.append("  </head>"+ret);
    String bodyOptions = _ejs.getOptions().getHtmlBody();
    if (bodyOptions.length()>0) code.append("  <body "+_ejs.getOptions().getHtmlBody()+">"+ret);
    else code.append("  <body>"+ret);
    code.append("    <h1>" + ((_javaName!=null) ? _simulationName : _packageName) + "</h1>"+ret);
    code.append("    <h2>" + res.getString("Generate.HtmlContents") + "</h2>"+ret);
    code.append("    <div class=\"contents\">"+ret);

    // Add an entry for each Introduction page created by the user
    String firstPageLink = null;
    Vector<Editor> pageList = _ejs.getDescriptionEditor().getPages();
    int counter = 0;
    int simTab = (_javaName!=null) ? -1 : _ejs.getSimInfoEditor().getSimulationTab();
    boolean simulationAdded = false;
        
    for (java.util.Enumeration<Editor> e = pageList.elements(); e.hasMoreElements(); ) {
      HtmlEditor htmlEditor = (HtmlEditor) e.nextElement();
      if (htmlEditor.isActive() && !htmlEditor.isInternal()) {
        counter++;
        if (counter==simTab && hasView) {
          simulationAdded = true;
          // And an extra link for the simulation itself!
          String link = (_ejs.getOptions().indexSimFile())? "index.html" : _simulationName+"_Simulation"+ localeSuffix + fileSuffix;
          code.append("      <div class=\"simulation\"><a href=\""+ link + "\" target=\"central\">" 
              + ((_javaName!=null) ? _ejs.getSimInfoEditor().getSimulationTabTitle() : _packageName) // + res.getString("Generate.HtmlSimulation")
              +"</a></div>"+ret);
          if (firstPageLink==null) firstPageLink = link;
        }
        LocaleItem item = _localeItem;
        OneHtmlPage htmlPage = htmlEditor.getHtmlPage(item);
        if (htmlPage==null) {
          if (!_localeItem.isDefaultItem()) htmlPage =  htmlEditor.getHtmlPage(item = LocaleItem.getDefaultItem());
        }
        if (htmlPage==null || htmlPage.isEmpty()) continue;
        String link;
        if (htmlPage.isExternal()) link = (_javaName!=null) ? _pathToLib + htmlPage.getLink() : _pathToLib + htmlPage.getPlainCode();
        else link = _simulationName + getIntroductionPageKey(counter,item) + ".html"; //(JSObfuscator.isGenerateXHTML() ? ".xhtml" : ".html");
        if (firstPageLink==null) firstPageLink = link;
        code.append("      <div class=\"intro\"><a href=\""+ link+"\" target=\"central\">" + htmlPage.getTitle()+"</a></div>"+ret);
      }
    } // end of for

    if (hasView && !simulationAdded) {
      String link = (_ejs.getOptions().indexSimFile())? "index.html" : _simulationName+"_Simulation"+ localeSuffix + fileSuffix;
      // And an extra link for the simulation itself!
      code.append("      <div class=\"simulation\"><a href=\""+ link+"\" target=\"central\">" 
          + ((_javaName!=null) ?  _ejs.getSimInfoEditor().getSimulationTabTitle() : _packageName) // + res.getString("Generate.HtmlSimulation")
          +"</a></div>"+ret);
      if (firstPageLink==null) firstPageLink = link;
    }
    code.append("    </div>"+ret); // End of contents
    // ---- Now the logo
    code.append("    <div class=\"signature\">"+ res.getString("Generate.HtmlEjsGenerated") + " "
        + "<a href=\"http://www.um.es/fem/EjsWiki\" target=\"_blank\">Easy Java Simulations</a></div>"+ret);
    code.append("  </body>"+ret);
    code.append("</html>"+ret);

    _htmlTable.put ("_Contents",code);

    // --- END OF the HTML page for the table of contents

    // --- The main HTML page
    code = new StringBuffer();
    if (JSObfuscator.isGenerateXHTML()) {
      code.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>"+ret);
      code.append("<!DOCTYPE html>"+ret);
      code.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:epub=\"http://www.idpf.org/2007/ops\">"+ret);
    }
    else code.append("<html>"+ret);
    code.append("  <head>"+ret);
    code.append("    <title> " + res.getString("Generate.HtmlFor") + " " + _simulationName + "</title>"+ret);
    code.append("  </head>"+ret);
    code.append("  <body>"+ret);
    if (left) code.append("    <frameset cols=\"25%,*\">"+ret);
    else      code.append("    <frameset rows=\"90,*\">"+ret);
    code.append("      <frame src=\""+_simulationName+"_Contents"+localeSuffix+fileSuffix+ "\" name=\"contents\" scrolling=\"auto\" target=\"_self\" />"+ret);
    if (firstPageLink!=null) {
      code.append("      <frame src=\""+firstPageLink+"\"");
    }
    else {
      String link = (_ejs.getOptions().indexSimFile())? "index.html" : _simulationName+"_Simulation"+ localeSuffix + fileSuffix;
      code.append("      <frame src=\""+link+"\"");
    }
    code.append(" name=\"central\" scrolling=\"auto\" target=\"_self\" />"+ret);
    code.append("      <noframes>"+ret);
    code.append("        Gee! Your browser is really old and doesn't support frames. You better update!!!"+ret);
    code.append("      </noframes>"+ret);
    code.append("    </frameset> "+ret);
    code.append("  </body>"+ret);
    code.append("</html>"+ret);

    _htmlTable.put ("",code);

    // --- An HTML page for the simulation itself
    if (hasView && _javaName!=null) {
      code = new StringBuffer();
      if (JSObfuscator.isGenerateXHTML()) {
        code.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>"+ret);
        code.append("<!DOCTYPE html>"+ret);
        code.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:epub=\"http://www.idpf.org/2007/ops\">"+ret);
      }
      else code.append("<html>"+ret);
      code.append("  <head>"+ret);
      //code.append("    <base href=\".\" />"+ret);
      code.append("    <title> " + res.getString("Generate.HtmlFor") + " " + _simulationName + "</title>"+ret);
      code.append("    <link rel=\"stylesheet\" type=\"text/css\" href=\""+_pathToLib+"_ejs_library/css/ejss.css\"></link>" + ret);
      code.append("  </head>"+ret);
      code.append("  <body "+_ejs.getOptions().getHtmlBody()+"> "+ret);
      code.append(generateHtmlForSimulation (_ejs, _simulationName, _javaName, _packageName, _pathToLib, _archiveStr));
      code.append("  </body>"+ret);
      code.append("</html>"+ret);

      _htmlTable.put ("_Simulation",code);
    }

    // And this makes the set of html files required for a frame configuration
  }

  static public String getIntroductionPageKey(int counter, LocaleItem localeItem) {
    return "_Intro_"+counter+ ( (localeItem==null || localeItem.isDefaultItem()) ? "" : "_"+localeItem.getKeyword());
  }
  
  /**
   * Adds the set of HTML files for the simulation description pages.
   * The key in the table is the name of the HTML file. The calling method must process these files.
   * @param _htmlTable Hashtable<String,StringBuffer> The table of HTML files
   * @param _ejs Osejs The calling Ejs
   * @param _simulationName String The name of the simulation
   * @param _filename String The base name for the HTML files (different from _simulationName if generating in a server)
   * @param _classpath String The classpath required to run the simulation.
   * @return Hashtable<String,String>
   * @throws IOException
   */
  static void addDescriptionHtml (Hashtable<String,StringBuffer> _htmlTable, Osejs _ejs, String _simulationName, String _pathToLib) {
    java.util.List<LocaleItem> localesDesired = getLocalesDesired(_ejs);
    for (LocaleItem item : localesDesired) addDescriptionHtml (_htmlTable, _ejs, item, _simulationName, _pathToLib);
  }

  static void addDescriptionHtml (Hashtable<String,StringBuffer> _htmlTable, Osejs _ejs, LocaleItem _localeItem,
      String _simulationName, String _pathToLib) {

    String ret = System.getProperty("line.separator");
    // --- An HTML page for each introduction page
    int counter = 0;
    for (java.util.Enumeration<Editor> e = _ejs.getDescriptionEditor().getPages().elements(); e.hasMoreElements(); ) {
      HtmlEditor htmlEditor = (HtmlEditor) e.nextElement();
      if (htmlEditor.isInternal()) continue;
      counter++;
      OneHtmlPage htmlPage = htmlEditor.getHtmlPage(_localeItem);
      if (htmlPage==null || htmlPage.isExternal() || htmlPage.isEmpty()) continue;
      StringBuffer code = new StringBuffer();
      code.append("<html>"+ret);
      code.append("  <head>"+ret);
      code.append("    <title> " + htmlPage.getTitle() + "</title>"+ret);
      code.append("    <link rel=\"stylesheet\" type=\"text/css\" href=\""+_pathToLib+"_ejs_library/css/ejss.css\"></link>" + ret);
      for (String filename : _ejs.getSimInfoEditor().getMoreCSSFiles()) {
        code.append("    <link rel=\"stylesheet\" type=\"text/css\" href=\""+filename+"\"></link>" + ret);
      }
      code.append("  </head>"+ret);
      code.append("  <body "+_ejs.getOptions().getHtmlBody()+"> "+ret);
      code.append(htmlPage.getHtmlCode (null));
      code.append("  </body>"+ret);
      code.append("</html>"+ret);
      _htmlTable.put (getIntroductionPageKey(counter,null),code);
    }
  }
  
  /**
   * Generates the code for the applet tag which includes the simulation in an HTML page
   * @param _ejs Osejs The calling EJS
   * @param _simulationName String The name of the simulation
   * @param _packageName String The package of the class
   * @param _classpath String The class path required to run the simulation as an applet
   * @return StringBuffer A StringBuffer with the code
   */
  static private String generateHtmlForSimulation (Osejs _ejs, String _simulationName, String _javaName, String _packageName,
      String _pathToLib, String _archiveStr) {
    String ret = System.getProperty("line.separator");
    String captureTxt = _ejs.getViewEditor().generateCode(Editor.GENERATE_CAPTURE_WINDOW,"").toString();
    StringBuffer code = new StringBuffer();
    code.append("    <div class=\"appletSection\">"+ret);
    if (captureTxt.trim().length()<=0) code.append("      <h3>"+res.getString("Generate.HtmlHereItIsNot")+"</h3>"+ret);
    else code.append("      <h3>"+res.getString("Generate.HtmlHereItIs")+"</h3>"+ret);
    code.append("      <applet code=\""+ _packageName + "." + _javaName+"Applet.class\"" +ret);

    if (_pathToLib.length()<=0) code.append("              codebase=\".\"");
    else code.append("              codebase=\""+_pathToLib+"\"");
    code.append(" archive=\""+_archiveStr +"\"" +ret);
    code.append("              name=\"" + _javaName + "\"  id=\"" + _javaName + "\""+ret);
    if (captureTxt.trim().length()<=0) code.append("              width=\"0\" height=\"0\"");
    else code.append("      "+captureTxt);

    code.append(">"+ret);
    code.append("        <param name=\"draggable\" value=\"true\" />"+ret);
    code.append("        <param name=\"permissions\" value=\"sandbox\" />"+ret);
    code.append("      </applet>"+ret);
    code.append("    </div>"+ret);

    //    if (_ejs.getOptions().experimentsEnabled() && _ejs.getExperimentEditor().getActivePageCount()>0) {
    //      code.append("    <div class=\"experimentsSection\">"+ret);
    //      code.append("      <h3>" + res.getString("Generate.TheExperiments")+"</h3>"+ret);
    //      code.append(_ejs.getExperimentEditor().generateCode(Editor.GENERATE_DECLARATION,_javaName));
    //      code.append("      <div class=\"killExperiment\"><a href=\"javascript:document."+_javaName+"._simulation.killExperiment();\">"+
    //                      ToolsRes.getString("MenuItem.KillExperiment") + "</a></div>"+ret);
    //      code.append("    </div>"+ret);
    //    }

//    code.append("      "+res.getString("Generate.HtmlHereComesTheJavaScript")+ret);
    code.append("    <div class=\"jsSection\">"+ret);
    code.append("      <h3>" + res.getString("Generate.HtmlJSControl")+"</h3>"+ret);
    code.append(jsCommand(_javaName,"Generate.HtmlPlay","_play()")+ret);
    code.append(jsCommand(_javaName,"Generate.HtmlPause","_pause()")+ret);
    code.append(jsCommand(_javaName,"Generate.HtmlReset","_reset()")+ret);
    code.append(jsCommand(_javaName,"Generate.HtmlStep","_step()")+ret);
    code.append(jsCommand(_javaName,"Generate.HtmlSlow","_setDelay(1000)")+ret);
    code.append(jsCommand(_javaName,"Generate.HtmlFast","_setDelay(100)")+ret);
    code.append(jsCommand(_javaName,"Generate.HtmlFaster","_setDelay(10)")+ret);
    //CJB for collaborative
    if(_ejs.getSimInfoEditor().addAppletColSupport())
      code.append(jsCommand(_javaName,"Generate.HtmlStartCollaboration","_simulation.startColMethod()")+ret);
    //CJB for collaborative
    code.append("    </div>"+ret);
    return code.toString();
  }

  static private String jsCommand (String _javaName, String _label, String _method) {
    //    return "<input type=\"BUTTON\" value=\"" + res.getString(_label)  +
    //      "\" onclick=\"document." + _javaName + "."+_method+";document." + _javaName + "._simulation.update();\";>";
    return "      <a href=\"javascript:document."+_javaName+"."+_method+";document."+_javaName+"._simulation.update();\">"+
    res.getString(_label)  + "</a> ";
  }

  
} // end of class
