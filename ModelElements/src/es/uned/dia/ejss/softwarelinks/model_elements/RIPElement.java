package es.uned.dia.ejss.softwarelinks.model_elements;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import org.colos.ejs.model_elements.AbstractModelElement;
import org.colos.ejs.model_elements.ModelElementsCollection;
import org.colos.ejs.osejs.edition.ModelEditor;
import org.opensourcephysics.display.OSPRuntime;

import es.uned.dia.ejss.softwarelinks.utils.RIPCodeBuilder;
import es.uned.dia.ejss.softwarelinks.utils.RIPConfigurationModel;
import es.uned.dia.softwarelinks.nodejs.RIPClient;
import es.uned.dia.softwarelinks.nodejs.RIPClient.RIPException;
import es.uned.dia.softwarelinks.nodejs.RIPInfo;
import es.uned.dia.softwarelinks.nodejs.RIPMetadata;
import es.uned.dia.softwarelinks.nodejs.RIPMethod;
import es.uned.dia.softwarelinks.transport.HttpTransport;

/**
 * Class to interact with a JIL Server
 * @author Adapted to Javascript from the original code of Jesús Chacón <jcsombria@gmail.com> by Jacobo Sáenz 
 */
public class RIPElement extends AbstractModelElement {
  private static ImageIcon ELEMENT_ICON = AbstractModelElement.createImageIcon("es/uned/dia/ejss/softwarelinks/resources/rip.png"); // This icon is included in this jar
  
  protected static final Object CONNECTION_OK = "Connected to ";
  protected static final Object INVALID_PATH = "Invalid Path or nothing there";
  protected static final Object SERVER_KO = "Server is not working";

  private RIPConfigurationModel configuration = new RIPConfigurationModel();
  class LinksTableModel extends DefaultTableModel {
    public final String[] COLUMNS = {"Server", "EJS", "get", "set"};

      public void setDataVector(Object[][] dataVector) {
        setDataVector(dataVector, COLUMNS);
      }
  
      public Class<?> getColumnClass(int c) {
        return getValueAt(0, c).getClass();
      }
  }
  
  //GUI elements
  private LinksTableModel linksTableModel;
  private DefaultTableModel methodsTableModel;
  private DefaultTableModel indicatorsTableModel;
  private DefaultTableModel controlsTableModel;
  private JTextField serverText = new JTextField("localhost", 20);
  private JTextField portText = new JTextField("2055", 6);
  private JTextField evaluableText = new JTextField("", 50);
  private JTextField labIdText = new JTextField();
  private JTextField labDescriptionText = new JTextField();
  private JTextArea callbackText = new JTextArea("", 4 , 23);
  private JTextArea initialCodeText = new JTextArea("", 4 , 23);
  //private JTextField viFileText = new JTextField("ripTest.vi", 32);  
   
  private JPopupMenu popupMenuEJS;  
   
  private JTable methodsTable;
  private JTable controlsTable;
  private JTable indicatorsTable;
  private JTable linksTable;
  private JPopupMenu popupMenuInd;
  private JPopupMenu popupMenuCon;
  
   // used to test the connection with the server
  RIPClient ripTest = null;
  private Component parent;

  private ModelEditor editor;

  private JPanel codePanel;

  private boolean showCodeTab;

  private JTabbedPane mainPanel;
  
  public RIPElement() { 
    // Server writable Table
    methodsTableModel = new DefaultTableModel();
    methodsTableModel.addColumn("Name");
    methodsTableModel.addColumn("Params");
    methodsTableModel.addColumn("Purpose");
          
    controlsTableModel = new DefaultTableModel();
    Vector<String> controlsColumns = new Vector<String>();
    controlsColumns.add("Name");
    controlsColumns.add("Type");
    Vector<Vector<String>> controlsVector = new Vector<Vector<String>>();
    controlsTableModel.setDataVector(controlsVector, controlsColumns);
    // Server readable Table
    indicatorsTableModel = new DefaultTableModel();
    Vector<String> indicatorsColumns = new Vector<String>();
    indicatorsColumns.add("Name");
    indicatorsColumns.add("Type");
    Vector<Vector<String>> indicatorsVector = new Vector<Vector<String>>();
    indicatorsTableModel.setDataVector(indicatorsVector, indicatorsColumns);
    // Links Table
    linksTableModel = new LinksTableModel();
    Object[][] data = {{"", "", new Boolean(false), new Boolean(true)}};
    linksTableModel.setDataVector(data);
  }
   
  // -------------------------------
  // Implementation of ModelElement
  // -------------------------------
  
  public ImageIcon getImageIcon() { return ELEMENT_ICON; }
  
  public String getGenericName() { return "RIP_Element"; }
  
  public String getConstructorName() { return "RIP"; }
  
  public String getInitializationCode(String _name) { // Code for the LINT in JS
    StringBuffer buffer = new StringBuffer();
    buffer.append("var RIP = {};");  
    return buffer.toString(); 
  } 

  public String getSourceCode(String name) { // Code that goes into the body of the model 
    RIPCodeBuilder builder = new RIPCodeBuilder();
    getServerInfo();
    builder.setName(name);
    builder.setModel(configuration);
    String code = builder.getCode();
    return code;
  }  

  
  public String getImportStatements() { // Required for Lint
    return "SoftwareLinks/RIP.js"; 
  }

  // -------------------------------
  // Help and edition
  // -------------------------------

  public String getTooltip() {
    return "provides access through Internet using the Remote Interoperability Protocol";
  }
  
  @Override
  protected String getHtmlPage() { 
    return "es/uned/dia/ejss/softwarelinks/resources/rip.html"; 
  }
  
  protected Component createEditor(String name, Component parentComponent, final ModelElementsCollection collection) {
    return createJilEditor(name, parentComponent, collection);
  }
  
  protected Component createJilEditor(String name, Component parentComponent, final ModelElementsCollection collection) {
    parent = parentComponent;
    editor = collection.getEJS().getModelEditor();

    mainPanel = new JTabbedPane();
    mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    mainPanel.setPreferredSize(new Dimension(600,300));

    JPanel serverPanel = createServerPanel(); 
    JPanel serverVariablesPanel = createServerVariablesPanel();
    JScrollPane linksPanel = createLinksPanel();
    codePanel = createCodePanel();
    mainPanel.addTab("Server", serverPanel);
    mainPanel.addTab("Reported Variables", serverVariablesPanel);
    mainPanel.addTab("Linked Variables", linksPanel);
//    mainPanel.addTab("Code", codePanel);

    return mainPanel;
  }
  
  private JPanel createServerPanel() {
    JPanel serverPanel = new JPanel();
    serverPanel.setBorder(new TitledBorder(null, "RIP server configuration", TitledBorder.LEADING, TitledBorder.TOP));
    serverPanel.setMinimumSize(new Dimension(620, 180));
    serverPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 180));
    serverPanel.setPreferredSize(new Dimension(620, 180));
    SpringLayout sl_topPanel = new SpringLayout();
    serverPanel.setLayout(sl_topPanel);
    
    JLabel serverLabel = new JLabel("Server address:");
    sl_topPanel.putConstraint(SpringLayout.NORTH, serverLabel, 7, SpringLayout.NORTH, serverPanel);
    sl_topPanel.putConstraint(SpringLayout.WEST, serverLabel, 5, SpringLayout.WEST, serverPanel);
    serverPanel.add(serverLabel);
    
    JLabel portLabel = new JLabel("Port:");
    sl_topPanel.putConstraint(SpringLayout.NORTH, portLabel, 0, SpringLayout.NORTH, serverLabel);
//    sl_topPanel.putConstraint(SpringLayout.EAST, portLabel, 126, SpringLayout.WEST, serverLabel);
    sl_topPanel.putConstraint(SpringLayout.WEST, portLabel, 10, SpringLayout.EAST, serverText);
    serverPanel.add(portLabel);
    
    JLabel labIdLabel = new JLabel("Lab Id:");
    sl_topPanel.putConstraint(SpringLayout.NORTH, labIdLabel, 6, SpringLayout.SOUTH, serverLabel);
    sl_topPanel.putConstraint(SpringLayout.EAST, labIdLabel, 0, SpringLayout.EAST, serverLabel);
    serverPanel.add(labIdLabel);
    
    JLabel labDescriptionLabel = new JLabel("Lab Description:");
    sl_topPanel.putConstraint(SpringLayout.NORTH, labDescriptionLabel, 6, SpringLayout.SOUTH, labIdLabel);
    sl_topPanel.putConstraint(SpringLayout.EAST, labDescriptionLabel, 0, SpringLayout.EAST, labIdLabel);
    serverPanel.add(labDescriptionLabel);

    
    sl_topPanel.putConstraint(SpringLayout.NORTH, serverText, -2, SpringLayout.NORTH, serverLabel);
    sl_topPanel.putConstraint(SpringLayout.WEST, serverText, 6, SpringLayout.EAST, serverLabel);
    serverPanel.add(serverText);
    serverText.setColumns(10);

    sl_topPanel.putConstraint(SpringLayout.NORTH, portText, -2, SpringLayout.NORTH, serverLabel);
    sl_topPanel.putConstraint(SpringLayout.WEST, portText, 6, SpringLayout.EAST, portLabel);
    sl_topPanel.putConstraint(SpringLayout.EAST, portText, -6, SpringLayout.EAST, serverPanel);
    serverPanel.add(portText);
    portText.setColumns(10);

    sl_topPanel.putConstraint(SpringLayout.NORTH, labIdText, 4, SpringLayout.SOUTH, serverText);
    sl_topPanel.putConstraint(SpringLayout.WEST, labIdText, 6, SpringLayout.EAST, labIdLabel);
    sl_topPanel.putConstraint(SpringLayout.EAST, labIdText, -6, SpringLayout.EAST, serverPanel);
    serverPanel.add(labIdText);
    labIdText.setColumns(10);
 
    sl_topPanel.putConstraint(SpringLayout.NORTH, labDescriptionText, 4, SpringLayout.SOUTH, labIdText);
    sl_topPanel.putConstraint(SpringLayout.WEST, labDescriptionText, 6, SpringLayout.EAST, labDescriptionLabel);
    sl_topPanel.putConstraint(SpringLayout.EAST, labDescriptionText, -6, SpringLayout.EAST, serverPanel);    
    serverPanel.add(labDescriptionText);
    
    JButton testButton = new JButton("Get metadata");
    sl_topPanel.putConstraint(SpringLayout.NORTH, testButton, 6, SpringLayout.SOUTH, labDescriptionText);
    sl_topPanel.putConstraint(SpringLayout.EAST, testButton, 0, SpringLayout.EAST, serverPanel);
    
    methodsTable = new JTable(methodsTableModel) {
      private static final long serialVersionUID = 1L;
      public boolean isCellEditable(int row, int col) { return false; }
    };      
    methodsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);    
    JScrollPane methodsScrollPane = new JScrollPane(methodsTable);
    sl_topPanel.putConstraint(SpringLayout.NORTH, methodsScrollPane, 4, SpringLayout.SOUTH, testButton);
    sl_topPanel.putConstraint(SpringLayout.SOUTH, methodsScrollPane, 4, SpringLayout.SOUTH, serverPanel);
    sl_topPanel.putConstraint(SpringLayout.EAST, methodsScrollPane, 4, SpringLayout.EAST, serverPanel);
    sl_topPanel.putConstraint(SpringLayout.WEST, methodsScrollPane, 4, SpringLayout.WEST, serverPanel);
    
    serverPanel.add(methodsScrollPane);
    
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
    AbstractAction testServer = new AbstractAction("Get Server Info"){
      private static final long serialVersionUID = 1L;
      public void actionPerformed(ActionEvent e) {
        boolean serverResponds = getServerInfo();
        if(serverResponds) {
          RIPMetadata meta = configuration.getMetadata();
          RIPInfo info = meta.getInfo();
          JOptionPane.showMessageDialog(parent, CONNECTION_OK + ((info != null) ? info.getName() : ""));
        } else {
          JOptionPane.showMessageDialog(parent, SERVER_KO);
        }
      }
    };
    testButton.setAction(testServer);

    serverPanel.add(testButton);
    return serverPanel;
  }

  private boolean getServerInfo() {
    try {
      String urlServer = "http://"+serverText.getText()+":"+portText.getText();
      HttpTransport transport = new HttpTransport(urlServer);
      ripTest = new RIPClient(transport);
      RIPMetadata meta = ripTest.info();
      RIPInfo info = meta.getInfo();
      if(info != null) {
        configuration.setMetadata(meta);
        labIdText.setText(info.getName());
        labDescriptionText.setText(info.getDescription());          
        methodsTableModel.setDataVector(new Object[][]{}, new Object[]{"Name", "Params", "Returns", "Purpose"});
        showCodeTab = false;
        for(RIPMethod method : meta.getMethods()) {
          String name = method.getName();
          if(name.equals("step") || name.equals("eval")) {
            showCodeTab = true;
          }
          Object[] row = { method.getName(), method.getParams().toString(), "n/a", method.getPurpose()};
          methodsTableModel.addRow(row);
        }
        if(codePanel != null) {
          int tabs = mainPanel.getTabCount();
          if(showCodeTab && tabs == 3) {
            mainPanel.addTab("Code", codePanel);
          }
          if(!showCodeTab && tabs == 4) {
            mainPanel.remove(3);
          }
        }
        Vector<String> columns = new Vector<String>();
        columns.add("Name"); columns.add("Type");
        Vector<Vector<String>> controls = mapToVector(meta.getWritable());
        Vector<Vector<String>> indicators = mapToVector(meta.getReadable());
        controlsTableModel.setDataVector(controls, columns);
        indicatorsTableModel.setDataVector(indicators, columns);
        return true;
      }
    } catch (MalformedURLException | RIPException e) {
      System.err.println("Error: can't get server info");
    }
    return false;
  }
  
  private JPanel createServerVariablesPanel() {
    JPanel ripPanel = new JPanel();
    ripPanel.setLayout(new BoxLayout(ripPanel, BoxLayout.X_AXIS));
    ripPanel.setBorder(new TitledBorder(null, "Server writable and readable variables", TitledBorder.LEADING, TitledBorder.TOP, null, null));
    ripPanel.setMinimumSize(new Dimension(620, 180));   
    ripPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
    ripPanel.setPreferredSize(new Dimension(620, 180));

    controlsTable = new JTable(controlsTableModel) {
      private static final long serialVersionUID = 1L;
      public boolean isCellEditable(int row, int col) { return false; }
    };      
    controlsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    
    JScrollPane controlsScrollPane = new JScrollPane(controlsTable); //an scroll panel for the table
    ripPanel.add(controlsScrollPane);
    controlsScrollPane.setBorder(BorderFactory.createTitledBorder("Server Writable Variables"));
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++    
    AbstractAction connectControlToVariable = new AbstractAction("Connect Writable to variable "){
      private static final long serialVersionUID = 1L;
      public void actionPerformed(ActionEvent e) {
        if(controlsTable.getSelectedRow() == -1) {
          JOptionPane.showMessageDialog(null, "Please, select a Writable Var.");
          return;           
        }
        String varname = (String)controlsTable.getValueAt(controlsTable.getSelectedRow(), 0),
            vartype = (String)controlsTable.getValueAt(controlsTable.getSelectedRow(), 1);         

        // Passing an empty String as the last parameter hides the default methods "_isPlaying, _isPaused, _isApplet"         
        //String variable = org.colos.ejs.osejs.utils.EditorForVariables.edit(collection.getEJS().getModelEditor(), "Variables", vartype, linksTable, varname, "");
        String variable = org.colos.ejs.osejs.utils.EditorForVariables.edit(editor, "Variables", "", linksTable, varname, ""); 
        if (variable != null) {
          Vector<Object> row = new Vector<>();
          row.add(varname);
          row.add(variable + " : " + vartype);
          row.add(false);
          row.add(true);
          linksTableModel.addRow(row);
        }
      }
    };

    popupMenuCon = new JPopupMenu();
    popupMenuCon.add(connectControlToVariable);
    controlsTable.addMouseListener (new MouseAdapter() {        
      public void mousePressed (MouseEvent _evt) {
        if (OSPRuntime.isPopupTrigger(_evt) && controlsTable.isEnabled ()) {
          int row = controlsTable.rowAtPoint(_evt.getPoint()); 
          if(row != -1) controlsTable.setRowSelectionInterval(row, row);
          popupMenuCon.show(_evt.getComponent(), _evt.getX(), _evt.getY());
        }
      }
    });    
    
    indicatorsTable = new JTable(indicatorsTableModel) {
      private static final long serialVersionUID = 1L;
      public boolean isCellEditable(int row, int col) { return false; }
    };
    indicatorsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    JScrollPane indicatorsScrollPane = new JScrollPane(indicatorsTable); //an scroll panel for the table
    ripPanel.add(indicatorsScrollPane);
    indicatorsScrollPane.setBorder(BorderFactory.createTitledBorder("Server Readable variables"));

    AbstractAction connectIndicatorToVariable = new AbstractAction("Connect Readable to Model Variable"){
      private static final long serialVersionUID = 1L;
      public void actionPerformed(ActionEvent e) {
        if(indicatorsTable.getSelectedRow() == -1) {
          JOptionPane.showMessageDialog(null, "Please, select an Readable Var.");
          return;           
        }
        String varname = (String)indicatorsTable.getValueAt(indicatorsTable.getSelectedRow(), 0),
               vartype = (String)indicatorsTable.getValueAt(indicatorsTable.getSelectedRow(), 1);         
        
        // Passing an empty String as the last parameter hides the default methods "_isPlaying, _isPaused, _isApplet"         
        //String variable = org.colos.ejs.osejs.utils.EditorForVariables.edit(collection.getEJS().getModelEditor(), "Variables", vartype, linksTable, varname, "");
        String variable = org.colos.ejs.osejs.utils.EditorForVariables.edit(editor, "Variables", "", linksTable, varname, "");
        if (variable != null) {
          Vector<Object> row = new Vector<>();
          row.add(varname);
          row.add(variable + " : " + vartype);
          row.add(true);
          row.add(false);
          linksTableModel.addRow(row);
        }
      }
    };

    popupMenuInd = new JPopupMenu();
    popupMenuInd.add(connectIndicatorToVariable);
    indicatorsTable.addMouseListener (new MouseAdapter() {
      public void mousePressed (MouseEvent _evt) {
        if (OSPRuntime.isPopupTrigger(_evt) && indicatorsTable.isEnabled ()) {
          int row = indicatorsTable.rowAtPoint(_evt.getPoint()); 
          if(row != -1) indicatorsTable.setRowSelectionInterval(row, row);
          popupMenuInd.show(_evt.getComponent(), _evt.getX(), _evt.getY());
        }
      }
    });

    return ripPanel;
  }
  
  private JPanel createCodePanel() {
    JPanel topEvaluablePanel = new JPanel();
    topEvaluablePanel.setBorder(new TitledBorder(null, "Functions to evaluate in the server ", TitledBorder.LEADING, TitledBorder.TOP));
    topEvaluablePanel.setMinimumSize(new Dimension(620, 180));
    topEvaluablePanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 180));
    topEvaluablePanel.setPreferredSize(new Dimension(620, 180));
    SpringLayout sl_topEvaluable = new SpringLayout();
    topEvaluablePanel.setLayout(sl_topEvaluable);
      
    JLabel evalLabel = new JLabel("Expression to Evaluate:");
    sl_topEvaluable.putConstraint(SpringLayout.NORTH, evalLabel, 7, SpringLayout.NORTH, topEvaluablePanel);
    sl_topEvaluable.putConstraint(SpringLayout.WEST, evalLabel, 7, SpringLayout.WEST, topEvaluablePanel);
    topEvaluablePanel.add(evalLabel);
      
    sl_topEvaluable.putConstraint(SpringLayout.NORTH, evaluableText, 4, SpringLayout.SOUTH, evalLabel);
    sl_topEvaluable.putConstraint(SpringLayout.WEST, evaluableText, 0, SpringLayout.WEST, evalLabel);
    topEvaluablePanel.add(evaluableText);
    evaluableText.setColumns(10);

//    JLabel callbackLabel = new JLabel("Action after Evaluation (Callback):");
//    sl_topEvaluable.putConstraint(SpringLayout.NORTH, callbackLabel, 7, SpringLayout.NORTH, topEvaluablePanel);
//    sl_topEvaluable.putConstraint(SpringLayout.WEST, callbackLabel, 7, SpringLayout.EAST, evalLabel);
//    topEvaluablePanel.add(callbackLabel);
//      
//    sl_topEvaluable.putConstraint(SpringLayout.NORTH, callbackText, 4, SpringLayout.SOUTH, callbackLabel);
//    sl_topEvaluable.putConstraint(SpringLayout.WEST, callbackText, 24, SpringLayout.EAST, evaluableText);
//    topEvaluablePanel.add(callbackText);
//    callbackText.setBorder(BorderFactory.createLineBorder(Color.GRAY));
      
      
    JLabel initialCodeLabel = new JLabel("Initial Code:");
    sl_topEvaluable.putConstraint(SpringLayout.NORTH, initialCodeLabel, 7, SpringLayout.NORTH, topEvaluablePanel);
    sl_topEvaluable.putConstraint(SpringLayout.WEST, initialCodeLabel, 87, SpringLayout.EAST, evalLabel);
    topEvaluablePanel.add(initialCodeLabel);
    sl_topEvaluable.putConstraint(SpringLayout.SOUTH, topEvaluablePanel, 4, SpringLayout.SOUTH, initialCodeText);
    sl_topEvaluable.putConstraint(SpringLayout.NORTH, initialCodeText, 4, SpringLayout.SOUTH, initialCodeLabel);
    sl_topEvaluable.putConstraint(SpringLayout.WEST, initialCodeText, 10, SpringLayout.EAST, evaluableText);
    sl_topEvaluable.putConstraint(SpringLayout.EAST, initialCodeText, -10, SpringLayout.EAST, topEvaluablePanel);
    topEvaluablePanel.add(initialCodeText);
//    initialCodeText.setBorder(BorderFactory.createLineBorder(Color.GRAY));
    //callbackText.setColumns(15);
    //callbackText.setSize(15,50);

    sl_topEvaluable.putConstraint(SpringLayout.NORTH, portText, -2, SpringLayout.NORTH, evalLabel);
    
    topEvaluablePanel.setVisible(showCodeTab);
    return topEvaluablePanel;
  }

  private JScrollPane createLinksPanel() {
    linksTable = new JTable(linksTableModel);
    linksTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    linksTable.getModel().addTableModelListener(new TableModelListener() {
      public void tableChanged(TableModelEvent e) {
        if (TableModelEvent.UPDATE == e.getType() && linksTable.getRowCount() == e.getLastRow()+1) {
          Object[] emptyRow = new Object[]{"", "", new Boolean(false), new Boolean(false)};
          linksTableModel.addRow(emptyRow);
        }
      }
    });
    AbstractAction connectVariable = new AbstractAction("Connect variable"){
      private static final long serialVersionUID = 1L;
      public void actionPerformed(ActionEvent e) {
        // Passing an empty String as the last parameter hides the default methods "_isPlaying, _isPaused, _isApplet"
        String variable = org.colos.ejs.osejs.utils.EditorForVariables.edit(editor, "Variables", null, linksTable, "", "");
        if (variable != null) {
            int row = linksTable.getSelectedRow(), column = 1;
          linksTableModel.setValueAt(variable, row, column);
        }
      }
    };
    AbstractAction deleteRow = new AbstractAction("Delete link"){
      private static final long serialVersionUID = 1L;
      public void actionPerformed(ActionEvent e) {
          int row = linksTable.getSelectedRow();
        linksTableModel.removeRow(row);
      }
    };
    popupMenuEJS = new JPopupMenu();
    popupMenuEJS.add(connectVariable);
    popupMenuEJS.add(deleteRow);
    linksTable.addMouseListener (new MouseAdapter() {       
      public void mousePressed (MouseEvent _evt) {
        if (OSPRuntime.isPopupTrigger(_evt) && linksTable.isEnabled ()) {
          int row = linksTable.rowAtPoint(_evt.getPoint()); 
          if(row != -1) { 
            linksTable.setRowSelectionInterval(row, row);
          }
          popupMenuEJS.show(_evt.getComponent(), _evt.getX(), _evt.getY());
        }
      }
    });    

    JScrollPane linksScrollPane = new JScrollPane(linksTable); //an scroll panel for the table
    linksScrollPane.setBorder(BorderFactory.createTitledBorder("Table of Linked Variables"));
    linksScrollPane.setMinimumSize(new Dimension(620, 180));
    linksScrollPane.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
    linksScrollPane.setPreferredSize(new Dimension(620, 180));

    TableModelListener ta = new TableModelListener(){ //Add a row to the table when last row is edited
      public void tableChanged(TableModelEvent e){
        if (TableModelEvent.UPDATE == e.getType() && linksTable.getRowCount() == e.getLastRow()+1){
          Object[] emptyRow = new Object[]{"", "", new Boolean(false), new Boolean(false)};
          linksTableModel.addRow(emptyRow);
        }       
      }       
    };
    linksTable.getModel().addTableModelListener(ta);

    return linksScrollPane;
  }
  
  private Vector<Vector<String>> mapToVector(Map<String, String> map) {
      Iterator<Map.Entry<String,String>> iter = map.entrySet().iterator();
      Vector<Vector<String>> result = new Vector<Vector<String>>();      
      while(iter.hasNext()) {
        Map.Entry<String,String> entry = iter.next();
        Vector<String> row = new Vector<String>();
        row.add(entry.getKey());
        row.add(entry.getValue());
        result.add(row);
      }
      return result;
  }

  public String savetoXML() {
    String server = serverText.getText().trim();
    String port = portText.getText().trim();
    String init = initialCodeText.getText();
    String step = evaluableText.getText();
    configuration.setInitCode((init != null) ? init : "");
    configuration.setStepCode((step != null) ? step : "");
    configuration.setServer(server, port);
    configuration.setData(linksTableModel.getDataVector());
    return configuration.dump();
  }

  /** 
   * Restores the states from an XML String
   */
  public void readfromXML(String inputXML) {
    configuration.restore(inputXML);
    serverText.setText(configuration.getServer());
    portText.setText(configuration.getPort());
    evaluableText.setText(configuration.getStepCode());
    initialCodeText.setText(configuration.getInitCode());
    linksTableModel.setDataVector(configuration.getData());
  }
}