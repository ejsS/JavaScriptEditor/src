package es.uned.dia.softwarelinks.nodejs;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class RIPMetadata {
  private static final String WRITABLE = "writable";
  private static final String READABLE = "readable";
  private static final String METHODS = "methods";
  private static final String INFO = "info";
  private List<RIPMethod> methods = new ArrayList<>();
  private Map<String, String> readable = new HashMap<>();
  private Map<String, String> writable = new HashMap<>();
  private RIPInfo info;
  
  public void parse(String input) {
    InputStream stream;
    try {
      stream = new ByteArrayInputStream(input.getBytes("UTF-8"));
      JsonReader reader = Json.createReader(stream);
      JsonObject message = reader.readObject();

      if(message.containsKey(INFO)) {
        JsonObject info = message.getJsonObject(INFO);
        this.info = new RIPInfo(info);
      }
      if(message.containsKey(METHODS)) {
        JsonObject methods = message.getJsonObject(METHODS);
        Iterator<String> i = methods.keySet().iterator();
        while(i.hasNext()) {
          String method = i.next();
          JsonObject info = methods.getJsonObject(method);
          this.methods.add(new RIPMethod(method, info));
        }
      } else {
        System.err.println("methods not found");
      }
      if(message.containsKey(WRITABLE)) {
        addDouble(message.getJsonArray(WRITABLE), this.writable);
      } else {
        System.err.println("writable not found");
      }
      if(message.containsKey(READABLE)) {
        addDouble(message.getJsonArray(READABLE), this.readable);
      } else {
        System.err.println("readable not found");
      }
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
  }  

  private void addDouble(JsonArray array, Map<String, String> map) {
    map.clear();
    for(Object item : array.toArray()) {
      map.put(item.toString(), "double");
    }
  }

  public RIPInfo getInfo() {
    return info;
  }

  public List<RIPMethod> getMethods() {
    return methods;
  }
  
  public Map<String, String> getReadable() {
    return readable;
  }
  
  public Map<String, String> getWritable() {
    return writable;
  }
  
  public String toString() {
    String toReturn = "";
    for(RIPMethod method : methods) {
      toReturn += method.toString() + "\n";
    }
    toReturn += "readable:\n  " + readable.toString() + "\n";
    toReturn += "writable:\n  " + writable.toString() + "\n";
    return toReturn;
  }
}