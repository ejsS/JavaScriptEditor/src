package es.uned.dia.softwarelinks.nodejs;

import java.util.HashMap;
import java.util.Map;

import javax.json.JsonObject;

public class RIPMethod {
  private String name = "method";
  private String purpose = "without purpose";
  private Map<String, String> params = new HashMap<>();
  
  public RIPMethod(String name, JsonObject info) {
    this.name = name;
    load(info);
  }
  
  private void load(JsonObject info) {
    if(info.containsKey("purpose")) {
      purpose = info.getString("purpose");
    }
    if(info.containsKey("params")) {
      loadParams(info.getJsonObject("params"));
    }
  }

  private void loadParams(JsonObject input) {
    String[] params = input.keySet().toArray(new String[]{});
    this.params.clear();
    for(String key : params) {
      this.params.put(key, input.getString(key));
    }
  }

  public String getName() {
    return name;
  }

  public String getPurpose() {
    return purpose;
  }

  public Map<String, String> getParams() {
    return params;
  }
  
  public String toString() {
    return "Method " + name + ":\n  purpose: " + purpose + "\n  params: " + params.toString();
  }
}