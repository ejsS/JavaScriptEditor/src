/**
 * RIPClient
 * author: Jesús Chacón <jcsombria@gmail.com>
 *
 * Copyright (C) 2013 Jesús Chacón
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uned.dia.softwarelinks.nodejs;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.List;
import java.util.ArrayList;

import javax.json.JsonValue;

import es.uned.dia.softwarelinks.rpc.JsonRpcClient;
import es.uned.dia.softwarelinks.rpc.RpcClient;
import es.uned.dia.softwarelinks.rpc.param.RpcParam;
import es.uned.dia.softwarelinks.rpc.param.RpcParamFactory;
import es.uned.dia.softwarelinks.transport.HttpTransport;
import es.uned.dia.softwarelinks.transport.Transport;

/**
 * Class to interact with a JIL Server
 * @author Jesús Chacón <jcsombria@gmail.com> 
 */
public class RIPClient implements RemoteInteroperabilityProtocol {
  //The names of the methods
  private static final String CONNECT = "connect";
  private static final String INFO = "info";
  private static final String SET = "set";
  private static final String GET = "get";
  private static final String OPEN = "open"; 
  private static final String RUN = "run"; 
  private static final String STOP = "stop"; 
  private static final String CLOSE = "close"; 
  private static final String DISCONNECT = "disconnect";

  private Transport transport;
  private RpcClient client;	
  private RIPMetadata metadata;

  public class RIPException extends Exception {
    public RIPException(Throwable e) {
      super(e);
    }
  }

  /**
   * Create a new RIPClient 
   * @param url The url of the server
   * @throws Exception 
   */
  public RIPClient(Transport transport) throws RIPException {
    try {
      this.transport = transport;
      client = new JsonRpcClient(transport);
    } catch (Exception e) {
      throw new RIPException(e);
    }
  }
	
  /**
   * Create a new RIPClient
   * @param url The url of the server
   * @throws Exception 
   */
  public RIPClient(String serverURL) throws RIPException {
    try {
      this.transport = new HttpTransport(serverURL);
      client = new JsonRpcClient(transport);
    } catch (Exception e) {
      throw new RIPException(e);
    }
  }

  @Override
  public boolean connect() {
    RpcParam<?>[] response = (RpcParam[])client.invoke(CONNECT, null);
    if(response == null) {
      return false;
    }
    // TO DO: do something with server response
    return true;
  }

  @Override
  public boolean disconnect() {
    RpcParam<?>[] response = (RpcParam[])client.invoke(DISCONNECT, null);
    if(response == null) {
      return false;
    }
    return true;
  }

  @Override
  public Object get(String[] name) {
    RpcParam<?>[] args = new RpcParam[] {
        RpcParamFactory.create("name", name)
    };
    RpcParam<?>[] result = (RpcParam[])client.invoke(GET, args);
    return result[0].get();
  }
    
  @Override
  public void set(String[] name, Object[] value) {
    RpcParam<?>[] args = new RpcParam[] {
        RpcParamFactory.create("name", name),
        RpcParamFactory.create("value", value),
    };
    client.notify(SET, args);
  }

  @Override
  public Object get(String name) {
    return get(new String[]{name});
  }

  @Override
  public void set(String name, Object value) {
    set(new String[]{name}, new Object[]{value});
  }

  @Override
  public RIPMetadata info() {
    if(metadata == null) {
      metadata = new RIPMetadata();
      RpcParam<String>[] response = (RpcParam<String>[])client.invoke(INFO, null);
      if(response != null) {
        String result = response[0].get();
        metadata.parse(result);
      }
    }
    return this.metadata;
  }

  @Override
  public boolean open(String element) {
    RpcParam<?>[] params = new RpcParam[] {
        RpcParamFactory.create("model", element),
    };
    RpcParam[] result = (RpcParam[])client.invoke(OPEN, params);
    return (boolean)result[0].get();
  }

  @Override
  public boolean run() {
    RpcParam[] result = (RpcParam[])client.invoke(RUN, null);
    return (boolean)result[0].get();
  }

  @Override
  public boolean stop() {
    RpcParam[] result = (RpcParam[])client.invoke(STOP, null);
    return (boolean)result[0].get();
  }

  @Override
  public boolean close() {
    RpcParam[] result = (RpcParam[])client.invoke(CLOSE, null);
    return (boolean)result[0].get();
  }

  @Override
  public boolean sync() {
    // TODO Auto-generated method stub
    return false;
  }
}